package com.datechsw.spellide.lang.majic.visitor;

import com.datechsw.spellide.lang.majic.psi.MajicElement;
import com.datechsw.spellide.lang.majic.psi.MajicObjectStatement;
import com.datechsw.spellide.lang.majic.psi.MajicTriggerStatement;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.progress.ProgressIndicatorProvider;
import com.intellij.psi.PsiElementVisitor;

/**
 * Base Visitor class for "Visitor Pattern"
 */
public class MajicElementVisitor extends PsiElementVisitor {
    private static final Logger log = Logger.getInstance(MajicElementVisitor.class);

    public void visitElement(MajicElement element) {
        log.info("visitElement: " + element.toString());
        ProgressIndicatorProvider.checkCanceled();
    }

    public void visitObjectStatement(MajicObjectStatement element) {
        log.info("visitObjectStatement: " + element.getName());
        visitElement(element);
    }

    public void visitTriggerStatement(MajicTriggerStatement element) {
        log.info("visitTriggerStatement: " + element.getSequenceNumber() + ":" + element.getName());
        visitElement(element);
    }
}
