package com.datechsw.spellide.lang.majic.psi.impl;

import com.datechsw.spellide.lang.majic.lexer.MajicTokenTypes;
import com.datechsw.spellide.lang.majic.psi.MajicObjectStatement;
import com.datechsw.spellide.lang.majic.psi.MajicTriggerStatement;
import com.datechsw.spellide.lang.majic.visitor.MajicElementVisitor;
import com.intellij.lang.ASTNode;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.util.IncorrectOperationException;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

/**
 * Implementation of PSI Named Element providing extra information about Majic Trigger definition
 */
public class MajicTriggerStatementImpl extends MajicElementImpl implements MajicTriggerStatement {
    private static final Logger log = Logger.getInstance(MajicTriggerStatementImpl.class);

    public MajicTriggerStatementImpl(@NotNull ASTNode node) {
        super(node);
    }
    
    public String toString() {
        return "TriggerStatement: " + getName();
    }

    @Override
    public PsiElement getNameElement() {
        PsiElement nameElement = findChildByType(MajicTokenTypes.IDENTIFIER);
        if (nameElement != null) nameElement = nameElement.getFirstChild();

        return nameElement;
    }

    @Override
    public void accept(MajicElementVisitor visitor) {
        visitor.visitTriggerStatement(this);
    }

    @Override
    public void accept(@NotNull PsiElementVisitor visitor) {
        if (visitor instanceof MajicElementVisitor) {
            ((MajicElementVisitor) visitor).visitTriggerStatement(this);
        } else {
            visitor.visitElement(this);
        }
    }
    
    @Override
    public String getName() {
        PsiElement nameElement = getNameElement();
        if (nameElement != null)
            return nameElement.getText();
        else
            return "unknown";
    }

    /**
     * Get Sequence number of this trigger definition. In case of any problem with getting sequence, 0 is returned
     * @return Sequence number
     */
    public int getSequenceNumber() {
        PsiElement elSeq = findChildByType(MajicTokenTypes.INTEGER_NUMBER);
        try {
            return Integer.parseInt(elSeq.getText());
        } catch (Exception ex) {
            return 0;
        }
    }

    /**
     * Return the neerest parent node with type MajicObjectStatement otherwise null if nothing found
     * @return Owning object
     */
    public MajicObjectStatement getOwningObject() {
        MajicObjectStatement retval = null;
        PsiElement current = this;
        int i = 1;
        do {
            current = current.getParent();
            if (current instanceof MajicObjectStatement) retval = (MajicObjectStatement)current;
        } while (retval == null && current != null);

        return retval;
    }

    /**
     * Get a hook (PRE_VALIDATE, POST_VALIDATE, POST_CI..), where this trigger is defined
     * @return Hook name
     */
    public String getHook() {
        return getFirstChild().getText();
    }

    @Override
    public PsiElement setName(@NonNls @NotNull String name) throws IncorrectOperationException{
        // TODO dodělat setName
        throw new IncorrectOperationException();
    }
}
