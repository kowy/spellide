package com.datechsw.spellide.lang.majic.psi.impl;

import com.datechsw.spellide.lang.majic.lexer.MajicTokenTypes;
import com.datechsw.spellide.lang.majic.psi.MajicObjectStatement;
import com.datechsw.spellide.lang.majic.psi.MajicTriggerStatement;
import com.datechsw.spellide.lang.majic.visitor.MajicElementVisitor;
import com.intellij.lang.ASTNode;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.util.IncorrectOperationException;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.List;

/**
 * Implementation of PSI Named Element providing extra information about Majic Object definition
 */
public class MajicObjectStatementImpl extends MajicElementImpl implements MajicObjectStatement {
    private static final Logger log = Logger.getInstance(MajicObjectStatementImpl.class);

    public MajicObjectStatementImpl(@NotNull ASTNode node) {
        super(node);
    }

    public String toString() {
        return "ObjectStatement: " + getName();
    }

    @Override
    public PsiElement getNameElement() {
        PsiElement nameElement = findChildByType(MajicTokenTypes.IDENTIFIER);

        return nameElement;
    }

    @Override
    public void accept(MajicElementVisitor visitor) {
        visitor.visitObjectStatement(this);
    }

    @Override
    public void accept(@NotNull PsiElementVisitor visitor) {
        if (visitor instanceof MajicElementVisitor) {
            ((MajicElementVisitor) visitor).visitObjectStatement(this);
        } else {
            visitor.visitElement(this);
        }
    }

    @Override
    public String getName() {
        PsiElement nameElement = getNameElement();
        if (nameElement != null)
            return nameElement.getText();
        else
            return "unknown";
    }

    public HashMap<Integer, MajicTriggerStatement> getTriggers() {
        HashMap<Integer, MajicTriggerStatement> retval;

        PsiElement triggerSection = this.findChildByType(MajicTokenTypes.TRIGGERS_SECTION);
        if (triggerSection != null && triggerSection instanceof MajicElementImpl) {
            List<PsiElement> triggers = ((MajicElementImpl)triggerSection).findChildrenByType(MajicTokenTypes.TRIGGER_DEFINITION);
            retval = new HashMap<Integer, MajicTriggerStatement>(triggers.size());
            for (PsiElement el : triggers) {
                if (el instanceof MajicTriggerStatement) {
                    if (!retval.containsKey(((MajicTriggerStatement) el).getSequenceNumber()))
                        retval.put(((MajicTriggerStatement) el).getSequenceNumber(), (MajicTriggerStatement)el);
                }
            }
        } else {
            retval = new HashMap<Integer, MajicTriggerStatement>(0);
        }

        return retval;
    }

    @Override
    public PsiElement setName(@NonNls @NotNull String name) throws IncorrectOperationException {
        // TODO dodělat setName
        throw new IncorrectOperationException();
    }
}
