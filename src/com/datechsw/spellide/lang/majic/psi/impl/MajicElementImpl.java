package com.datechsw.spellide.lang.majic.psi.impl;

import com.datechsw.spellide.lang.majic.MajicLanguage;
import com.datechsw.spellide.lang.majic.psi.MajicElement;
import com.datechsw.spellide.lang.majic.visitor.MajicElementVisitor;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import com.intellij.lang.Language;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Base element of the Spell PSI infrastructure
 */
public class MajicElementImpl extends ASTWrapperPsiElement implements MajicElement {

    public static final String IMPL_EXTENSION = "Impl";

    public MajicElementImpl(@NotNull ASTNode node) {
        super(node);
    }


    @Override
    public void accept(MajicElementVisitor visitor) {
        visitor.visitElement(this);
    }

    @Override
    public void accept(@NotNull PsiElementVisitor visitor) {
        visitor.visitElement(this);
    }

    @NotNull
    @Override
    public Language getLanguage() {
        return MajicLanguage.INSTANCE;
    }

    @Override
    public String toString() {
        String name = getClass().getName();
        if (name.endsWith(IMPL_EXTENSION)) {
            name = name.substring(0, name.length() - IMPL_EXTENSION.length());
        }

        name = name.substring(name.lastIndexOf('.') + 1);

        return name;
    }

    /**
     * Expose protected method from ASTDelegatePsiElementClass
     */
    public <T extends PsiElement> List<T> findChildrenByType(IElementType elementType) {
        return super.findChildrenByType(elementType);
    }
}