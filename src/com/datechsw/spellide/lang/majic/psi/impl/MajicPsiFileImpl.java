package com.datechsw.spellide.lang.majic.psi.impl;

import com.datechsw.spellide.lang.majic.MajicFileType;
import com.datechsw.spellide.lang.majic.psi.MajicPsiFile;
import com.intellij.extapi.psi.PsiFileBase;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.psi.FileViewProvider;
import org.jetbrains.annotations.NotNull;

/**
 * Implementation of a top level element of AST tree - PsiFileBase
 */
public class MajicPsiFileImpl extends PsiFileBase implements MajicPsiFile {

    public MajicPsiFileImpl(FileViewProvider viewProvider) {
        super(viewProvider, MajicFileType.INSTANCE.getLanguage());
    }

    @NotNull
    @Override
    public FileType getFileType() {
        return MajicFileType.INSTANCE;
    }
}