package com.datechsw.spellide.lang.majic.psi;

import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiNamedElement;

/**
 * Interface for PSI Named Element providing extra information about Majic Trigger definition
 */
public interface MajicTriggerStatement extends MajicElement, PsiNamedElement {
    PsiElement getNameElement();
    MajicObjectStatement getOwningObject();
    
    int getSequenceNumber();
    String getHook();
}
