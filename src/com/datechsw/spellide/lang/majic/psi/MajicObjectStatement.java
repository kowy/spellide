package com.datechsw.spellide.lang.majic.psi;

import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiNamedElement;

import java.util.HashMap;

/**
 * Interface for PSI Named Element providing extra information about Majic Object definition
 */
public interface MajicObjectStatement extends MajicElement, PsiNamedElement {
    PsiElement getNameElement();

    public HashMap<Integer, MajicTriggerStatement> getTriggers();
}