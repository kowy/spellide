package com.datechsw.spellide.lang.majic.psi;

import com.datechsw.spellide.lang.majic.visitor.MajicElementVisitor;
import com.intellij.psi.PsiElement;
import com.intellij.psi.tree.IElementType;

import java.util.List;

/**
 * Interface for a base element of the Spell PSI infrastructure
 */
public interface MajicElement extends PsiElement {
    void accept(MajicElementVisitor visitor);

    public <T extends PsiElement> List<T> findChildrenByType(IElementType elementType);
}
