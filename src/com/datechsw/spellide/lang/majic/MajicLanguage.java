package com.datechsw.spellide.lang.majic;

import com.datechsw.spellide.lang.majic.highlighter.MajicSyntaxHighlighter;
import com.intellij.lang.Language;
import com.intellij.openapi.fileTypes.SingleLazyInstanceSyntaxHighlighterFactory;
import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import com.intellij.openapi.fileTypes.SyntaxHighlighterFactory;
import org.jetbrains.annotations.NotNull;

/**
 * Top level configuration of Majic highlighter, language, file type etc.
 */
public class MajicLanguage extends Language {
    @NotNull
    public static final MajicLanguage INSTANCE = new MajicLanguage();
    public static final String LANG_NAME = "Majic";

    private MajicLanguage() {
        super(LANG_NAME, MajicFileType.MIME_TYPE);
        SyntaxHighlighterFactory.LANGUAGE_FACTORY.addExplicitExtension(this, new SingleLazyInstanceSyntaxHighlighterFactory() {
            @NotNull
            @Override
            protected SyntaxHighlighter createHighlighter() {
                return new MajicSyntaxHighlighter();
            }
        });
    }


}
