package com.datechsw.spellide.lang.majic.parser;

import com.datechsw.spellide.SpellIdeBundle;
import com.intellij.lang.*;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.Key;
import com.intellij.psi.tree.IElementType;
import com.intellij.psi.tree.TokenSet;
import com.intellij.util.diff.FlyweightCapableTreeStructure;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.PropertyKey;

/**
 * Own implementation of PsiBuilder providing some extended functions to simplify parsing:
 * * call error
 * * mark and advance
 * * check and advance
 */
public class MajicPsiBuilder
{
    private PsiBuilder builder;
    @NonNls
    private static final String PATH_TO_BUNDLE = "messages.SpellIdeBundle";

    public MajicPsiBuilder(@NotNull PsiBuilder builder) {
        this.builder = builder;
    }

    public void error(@PropertyKey(resourceBundle =  PATH_TO_BUNDLE) String message, Object... params) {
        builder.error(SpellIdeBundle.message(message, params));
    }

    public void error(PsiBuilder.Marker marker, @PropertyKey(resourceBundle = PATH_TO_BUNDLE) String message, Object... params) {
        marker.error(SpellIdeBundle.message(message, params));
    }

    public void errorToken(@PropertyKey(resourceBundle = PATH_TO_BUNDLE) String message, Object... params) {
        final PsiBuilder.Marker errMarker = builder.mark();
        builder.advanceLexer();
        errMarker.error(SpellIdeBundle.message(message, params));
        errMarker.drop();
    }

    /**
     * Check if current token is among possibleTypes. If yes, it will advance lexer to next token,
     * otherwise it will set error with message from resource bundle
     * @param message Error message, which will be shown if none of possibleTypes can be applyed. It is a key to resource bundle
     * @param markerToBreak If instance of some marker will be provided, it will be dropped
     * @param possibleTypes Current token should be one of these tokenTypes
     */
    public void checkAndAdvance(@PropertyKey(resourceBundle = PATH_TO_BUNDLE) String message,
                                @Nullable PsiBuilder.Marker markerToBreak,
                                IElementType... possibleTypes) {
        boolean check = false;
        for (IElementType el : possibleTypes) {
            check |= builder.getTokenType() == el;
        }

        if (!check) {
            if (markerToBreak == null) error(message);
            else {
                error(markerToBreak, message);
            }
        }

        builder.advanceLexer();
    }

    /**
     * Check if current token is among possibleTypes. If yes, mark it as type specified in marker parameter and continue.
     * Otherwise it will set error with message from resource bundle
     * @param message Error message, which will be shown if none of possibleTypes can be applyed. It is a key to resource bundle
     * @param markerType New IElementType which should overwrite the old one
     * @param possibleTypes Current token should be one of these tokenTypes
     */
    public void markAndAdvance(@PropertyKey(resourceBundle = PATH_TO_BUNDLE) String message,
                                IElementType markerType,
                                IElementType... possibleTypes) {
        final PsiBuilder.Marker mark = builder.mark();
        boolean check = false;
        for (IElementType el : possibleTypes) {
            check |= builder.getTokenType() == el;
        }

        if (!check) {
            error(message);
        } else {
            mark.done(markerType);
        }

        builder.advanceLexer();
    }

    /**
     * At first advance lexer and then check if a new token is among possibleTypes. If not,
     * it will set error with message from resource bundle
     * @param message Error message, which will be shown if none of possibleTypes can be applyed. It is a key to resource bundle
     * @param markerToBreak If instance of some marker will be provided, it will be dropped
     * @param possibleTypes Next token should be one of these tokenTypes
     */
    public void advanceAndCheck(@PropertyKey(resourceBundle = PATH_TO_BUNDLE) String message,
                                @Nullable PsiBuilder.Marker markerToBreak,
                                IElementType... possibleTypes) {
        builder.advanceLexer();

        boolean check = false;
        for (IElementType el : possibleTypes) {
            check |= builder.getTokenType() == el;
        }

        if (!check) {
            if (markerToBreak == null) error(message);
            else {
                error(markerToBreak, message);
            }
        }
    }

    /**
     * Conditionally advance lexer. If current token has one of possibleTypes, advance lexer.
     * @param possibleTypes List of possible token types
     */
    public void advanceLexerCond(IElementType... possibleTypes) {
        boolean check = false;
        for (IElementType el : possibleTypes) {
            check |= builder.getTokenType() == el;
        }

        if (check) builder.advanceLexer();
    }

    /**
     * Conditionally advance lexer. If NEXT token has one of possibleTypes, advance lexer.
     * @param possibleTypes List of possible token types
     */
    public void advanceLexerCondAhead(IElementType... possibleTypes) {
        boolean check = false;
        for (IElementType el : possibleTypes) {
            check |= builder.lookAhead(1) == el;
        }

        if (check) builder.advanceLexer();
    }


    // <editor-fold desc="Base methods">
    public Project getProject() {
        return builder.getProject();
    }

    public CharSequence getOriginalText() {
        return builder.getOriginalText();
    }

    public void advanceLexer() {
        builder.advanceLexer();
    }

    public IElementType getTokenType() {
        return builder.getTokenType();
    }

    public void setTokenTypeRemapper(@Nullable ITokenTypeRemapper remapper) {
        builder.setTokenTypeRemapper(remapper);
    }

    public void remapCurrentToken(IElementType type) {
        builder.remapCurrentToken(type);
    }

    public void setWhitespaceSkippedCallback(WhitespaceSkippedCallback callback) {
        builder.setWhitespaceSkippedCallback(callback);
    }

    public IElementType lookAhead(int steps) {
        return builder.lookAhead(steps);
    }

    public IElementType rawLookup(int steps) {
        return builder.rawLookup(steps);
    }

    public int rawTokenTypeStart(int steps) {
        return builder.rawTokenTypeStart(steps);
    }

    public String getTokenText() {
        return builder.getTokenText();
    }

    public int getCurrentOffset() {
        return builder.getCurrentOffset();
    }

    public PsiBuilder.Marker mark() {
        return builder.mark();
    }

    public boolean eof() {
        return builder.eof();
    }

    public ASTNode getTreeBuilt() {
        return builder.getTreeBuilt();
    }

    public FlyweightCapableTreeStructure<LighterASTNode> getLightTree() {
        return builder.getLightTree();
    }

    public void setDebugMode(boolean dbgMode) {
        builder.setDebugMode(dbgMode);
    }

    public void enforceCommentTokens(TokenSet tokens) {
        builder.enforceCommentTokens(tokens);
    }

    public LighterASTNode getLatestDoneMarker() {
        return builder.getLatestDoneMarker();
    }

    public <T> T getUserData(@NotNull Key<T> key) {
        return builder.getUserData(key);
    }

    public <T> void putUserData(@NotNull Key<T> key, @Nullable T value) {
        builder.putUserData(key, value);
    }

    public <T> T getUserDataUnprotected(@NotNull Key<T> key) {
        return builder.getUserDataUnprotected(key);
    }

    public <T> void putUserDataUnprotected(@NotNull Key<T> key, @Nullable T value) {
        builder.putUserDataUnprotected(key, value);
    }
    //</editor-fold>
}
