package com.datechsw.spellide.lang.majic.parser;

import com.datechsw.spellide.lang.majic.lexer.MajicTokenTypes;
import com.datechsw.spellide.lang.majic.psi.impl.MajicElementImpl;
import com.datechsw.spellide.lang.majic.psi.impl.MajicObjectStatementImpl;
import com.datechsw.spellide.lang.majic.psi.impl.MajicTriggerStatementImpl;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.tree.IElementType;

/**
 * Class converting AST Nodes to PSI node implementations.
 */
public class MajicPsiCreator {
    public static PsiElement createElement(ASTNode node) {
        final IElementType elType = node.getElementType();

        if (elType == MajicTokenTypes.TRIGGER_DEFINITION) {
            return new MajicTriggerStatementImpl(node);
        } else if (elType == MajicTokenTypes.OBJECT_DEFINITION) {
            return new MajicObjectStatementImpl(node);
        } else {
            return new MajicElementImpl(node);
        }
    }
}
