package com.datechsw.spellide.lang.majic.parser;

import com.datechsw.spellide.lang.majic.lexer.MajicLexer;
import com.datechsw.spellide.lang.majic.lexer.MajicTokenTypes;
import com.datechsw.spellide.lang.majic.psi.impl.MajicPsiFileImpl;
import com.intellij.lang.ASTNode;
import com.intellij.lang.ParserDefinition;
import com.intellij.lang.PsiParser;
import com.intellij.lexer.Lexer;
import com.intellij.openapi.project.Project;
import com.intellij.psi.FileViewProvider;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.tree.IFileElementType;
import com.intellij.psi.tree.TokenSet;
import org.jetbrains.annotations.NotNull;

/**
 * Initialization and configuration of Majic parser
 */
public class MajicParserDefinition implements ParserDefinition {
    @NotNull
    @Override
    public Lexer createLexer(Project project) {
        return new MajicLexer();
    }

    @Override
    public PsiParser createParser(Project project) {
        return new MajicParser();
    }

    @Override
    public IFileElementType getFileNodeType() {
        return MajicTokenTypes.FILE;
    }

    @NotNull
    @Override
    public TokenSet getWhitespaceTokens() {
        return MajicTokenTypes.WHITESPACES;
    }

    @NotNull
    @Override
    public TokenSet getCommentTokens() {
        return MajicTokenTypes.COMMENTS;
    }

    @NotNull
    @Override
    public TokenSet getStringLiteralElements() {
        return TokenSet.EMPTY;
    }

    @NotNull
    @Override
    public PsiElement createElement(ASTNode node) {
        final PsiElement element = MajicPsiCreator.createElement(node);

        return element;
    }

    @Override
    public PsiFile createFile(FileViewProvider viewProvider) {
        return new MajicPsiFileImpl(viewProvider);
    }

    @Override
    public SpaceRequirements spaceExistanceTypeBetweenTokens(ASTNode left, ASTNode right) {
        return SpaceRequirements.MAY;
    }
}