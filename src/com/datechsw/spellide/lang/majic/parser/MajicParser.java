package com.datechsw.spellide.lang.majic.parser;

import com.datechsw.spellide.SpellIdeBundle;
import com.datechsw.spellide.lang.majic.lexer.MajicTokenTypes;
import com.intellij.lang.ASTNode;
import com.intellij.lang.PsiBuilder;
import com.intellij.lang.PsiParser;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NotNull;

import java.util.Stack;

/**
 * Implementation of Majic parser. Here is built an AST Tree from Lexer output
 */
public class MajicParser implements PsiParser{
    private static final Logger log = Logger.getInstance(MajicParser.class);

    @NotNull
    @Override
    public ASTNode parse(IElementType root, PsiBuilder b) {
        final MajicPsiBuilder builder = new MajicPsiBuilder(b);
        final PsiBuilder.Marker rootMarker = builder.mark();
                

        while (!builder.eof()) {
            if (log.isDebugEnabled()) log.debug("parse: " + builder.getTokenText());
            if (builder.getTokenType() == MajicTokenTypes.KEYWORD_OBJECT) {
                parseObjectDefinition(builder);
            } else if (builder.getTokenType() == MajicTokenTypes.KEYWORD_MODIFY) {
                parseModifyDefinition(builder);
            } else {
                builder.error("parser.misplaced.keyword", builder.getTokenText());
                builder.advanceLexer();
            }

            builder.checkAndAdvance("parser.expected.semicolon", null, MajicTokenTypes.SEMICOLON);
        }

        rootMarker.done(root);
        return builder.getTreeBuilt();
    }

    /**
     * Parser for the main OBJECT definition
     */
    private void parseObjectDefinition(MajicPsiBuilder builder) {
        if (log.isDebugEnabled()) log.debug("parseObjectDefinition: " + builder.getTokenType());
        final PsiBuilder.Marker objMarker = builder.mark();

        builder.advanceLexer();  // eat OBJECT keyword
        builder.checkAndAdvance("parser.expected.identifier", null, MajicTokenTypes.IDENTIFIER);
        builder.checkAndAdvance("parser.expected.left_brace", null, MajicTokenTypes.LEFT_CURL_BRACKET, MajicTokenTypes.LEFT_CURL_BRACKET_SS);

        while (!builder.eof() && builder.getTokenType() != MajicTokenTypes.RIGHT_CURL_BRACKET) {
            log.debug("parseObjectDefinitionInner: " + builder.getTokenText());
            if (builder.getTokenType() == MajicTokenTypes.KEYWORD_TRIGGERS) {
                parseTriggerSection(builder);
            } else if (builder.getTokenType() == MajicTokenTypes.KEYWORD_ATTRIBUTES) {
                parseAttributesSection(builder);
            } else if (builder.getTokenType() == MajicTokenTypes.KEYWORD_METHODS) {
                parseMethodsSection(builder);
            } else if (builder.getTokenType() == MajicTokenTypes.KEYWORD_FACTORY) {
                parseFactorySection(builder);
            } else if (builder.getTokenType() == MajicTokenTypes.KEYWORD_DISPLAY_GROUP) {
                builder.advanceLexer(); // eat keyword
                builder.checkAndAdvance("parser.expected.string", null, MajicTokenTypes.STRING);
            } else if (builder.getTokenType() == MajicTokenTypes.KEYWORD_TENANT_REQUIRED ||
                       builder.getTokenType() == MajicTokenTypes.KEYWORD_TENANT_OPTIONAL ) {
                builder.advanceLexer(); // eat keyword
            } else {
                builder.error("parser.misplaced.keyword", builder.getTokenText());
                builder.advanceLexer();
            }

            builder.checkAndAdvance("parser.expected.semicolon", null, MajicTokenTypes.SEMICOLON);
        }

        if (builder.eof()) {
            builder.error(objMarker, "parser.expected.right_brace");
            objMarker.drop();
        } else {
            builder.advanceLexer();
            objMarker.done(MajicTokenTypes.OBJECT_DEFINITION);
        }
    }

    /**
     * Parser for MODIFY definition
     */
    private void parseModifyDefinition(MajicPsiBuilder builder) {
        if (log.isDebugEnabled()) log.debug("parseModifyDefinition: " + builder.getTokenType());
        final PsiBuilder.Marker modifyMarker = builder.mark();

        builder.advanceLexer();  // eat MODIFY keyword
        builder.checkAndAdvance("parser.expected.identifier", null, MajicTokenTypes.IDENTIFIER);
        builder.checkAndAdvance("parser.expected.identifier", null, MajicTokenTypes.IDENTIFIER);
        if (builder.getTokenType() == MajicTokenTypes.LEFT_CURL_BRACKET || builder.getTokenType() == MajicTokenTypes.LEFT_CURL_BRACKET_SS) {
            builder.advanceLexer(); // eat left bracket
            parseAdvancedTypeSimple(builder, true);
        } else {
            parseAdvancedTypeOneLine(builder, true, false);
        }

        modifyMarker.done(MajicTokenTypes.MODIFY_DEFINITION);
    }

    /**
     * Parser for main ATTRIBUTES section in object
     */
    private void parseAttributesSection(MajicPsiBuilder builder) {
        if (log.isDebugEnabled()) log.debug("parseAttributeSection: " + builder.getTokenText());
        builder.advanceLexer(); // eat ATTRIBUTES keyword
        final PsiBuilder.Marker attrSectMarker = builder.mark();
        builder.advanceLexerCond(MajicTokenTypes.IDENTIFIER);
        builder.advanceLexerCond(MajicTokenTypes.KEYWORD_SECONDARY);
        builder.checkAndAdvance("parser.expected.left_brace", null, MajicTokenTypes.LEFT_CURL_BRACKET, MajicTokenTypes.LEFT_CURL_BRACKET_SS);

        while (!builder.eof() && builder.getTokenType() != MajicTokenTypes.RIGHT_CURL_BRACKET) {
            parseAttribute(builder);
        }

        if (builder.eof()) {
            builder.error(attrSectMarker, "parser.expected.right_brace");
            attrSectMarker.drop();
        } else {
            builder.advanceLexer();
            attrSectMarker.done(MajicTokenTypes.ATTRIBUTES_SECTION);
        }
    }

    /**
     * Parser for one attribute definition
     * @param builder
     */
    private void parseAttribute(MajicPsiBuilder builder) {
        if (log.isDebugEnabled()) log.debug("parseAttribute: " + builder.getTokenText());
        final PsiBuilder.Marker attrMarker = builder.mark();
        builder.checkAndAdvance("parser.expected.identifier", null, MajicTokenTypes.IDENTIFIER);
        builder.advanceLexerCond(MajicTokenTypes.IDENTIFIER);  // name on virtual DB level
        builder.advanceLexerCond(MajicTokenTypes.KEYWORD_LOCAL);

        final PsiBuilder.Marker typeMarker = builder.mark();
        if (builder.getTokenType() == MajicTokenTypes.KEYWORD_BREL) {
            parseBrelType(builder);
        } else if (builder.getTokenType() == MajicTokenTypes.KEYWORD_QREL) {
            parseQrelType(builder);
        } else if (builder.getTokenType() == MajicTokenTypes.KEYWORD_SREL) {
            parseSrelType(builder);
        } else if (builder.getTokenType() == MajicTokenTypes.KEYWORD_UUID ||
                   builder.getTokenType() == MajicTokenTypes.KEYWORD_STRING ||
                   builder.getTokenType() == MajicTokenTypes.KEYWORD_INTEGER ||
                   builder.getTokenType() == MajicTokenTypes.KEYWORD_DATE ||
                   builder.getTokenType() == MajicTokenTypes.KEYWORD_DURATION) {
            parseSimpleType(builder);
        } else if (builder.getTokenType() == MajicTokenTypes.KEYWORD_DERIVED) {
            parseDerivedType(builder);
        } else {
            builder.error("parser.unknown_type", builder.getTokenText());
            builder.advanceLexer();
        }

        if (builder.getTokenType() == MajicTokenTypes.SEMICOLON) {
            typeMarker.done(MajicTokenTypes.ATTRIBUTE_TYPE);
            builder.advanceLexer();
        } else {
            builder.error("parser.expected.semicolon");
            typeMarker.drop();
        }

        attrMarker.done(MajicTokenTypes.ATTRIBUTE_DEFINITION);
    }

    /**
     * Parser for all simply types (UUID, STRING, INT...)
     */
    private void parseSimpleType(MajicPsiBuilder builder) {
        if (log.isDebugEnabled()) log.debug("parseSimpleType: " + builder.getTokenText());
        if (builder.getTokenType() == MajicTokenTypes.KEYWORD_UUID) builder.advanceLexerCondAhead(MajicTokenTypes.KEYWORD_OWNER_ATTR);
        builder.advanceLexer(); // eat type keyword
        builder.advanceLexerCond(MajicTokenTypes.KEYWORD_REQUIRED);
        if (builder.getTokenType() == MajicTokenTypes.LEFT_CURL_BRACKET) {
            builder.advanceLexer(); // eat left curl bracket
            parseAdvancedTypeSimple(builder, false);
        }
    }

    /**
     * Parser for SREL type definition
     */
    private void parseSrelType(MajicPsiBuilder builder) {
        if (log.isDebugEnabled()) log.debug("parseSrelType: " + builder.getTokenText());
        builder.advanceLexer(); // eat SREL keyword
        builder.checkAndAdvance("parser.expected.identifier", null, MajicTokenTypes.IDENTIFIER);
        builder.advanceLexerCond(MajicTokenTypes.KEYWORD_REQUIRED);
        if (builder.getTokenType() == MajicTokenTypes.LEFT_CURL_BRACKET) {
            builder.advanceLexer(); // eat left curl bracket
            parseAdvancedTypeSimple(builder, false);
        }
    }

    /**
     * Parser for BREL type defintion
     */
    private void parseBrelType(MajicPsiBuilder builder) {
        if (log.isDebugEnabled()) log.debug("parseBrelType: " + builder.getTokenText());
        builder.advanceLexer(); // eat BREL keyword
        builder.checkAndAdvance("parser.expected.identifier", null, MajicTokenTypes.IDENTIFIER);
        builder.checkAndAdvance("parser.expected.identifier", null, MajicTokenTypes.IDENTIFIER);
        builder.advanceLexerCond(MajicTokenTypes.KEYWORD_DYNAMIC, MajicTokenTypes.KEYWORD_STATIC);
        if (builder.getTokenType() == MajicTokenTypes.LEFT_CURL_BRACKET) {
            builder.advanceLexer(); // eat left curl bracket
            parseAdvancedTypeMultiRel(builder, MajicTokenTypes.KEYWORD_BREL);
        }
    }

    /**
     * Parser for QREL type defintion
     */
    private void parseQrelType(MajicPsiBuilder builder) {
        if (log.isDebugEnabled()) log.debug("parseQrelType: " + builder.getTokenText());
        builder.advanceLexer(); // eat QREL keyword
        builder.checkAndAdvance("parser.expected.identifier", null, MajicTokenTypes.IDENTIFIER);
        builder.advanceLexerCond(MajicTokenTypes.KEYWORD_DYNAMIC, MajicTokenTypes.KEYWORD_STATIC);
        if (builder.getTokenType() == MajicTokenTypes.LEFT_CURL_BRACKET) {
            builder.advanceLexer(); // eat left curl bracket
            parseAdvancedTypeMultiRel(builder, MajicTokenTypes.KEYWORD_QREL);
        }
    }

    /**
     * Parser for DERIVED type definition
     */
    private void parseDerivedType(MajicPsiBuilder builder) {
        if (log.isDebugEnabled()) log.debug("parseDerivedType: " + builder.lookAhead(1));
        PsiBuilder.Marker derivedMarker = builder.mark();
        builder.advanceAndCheck("parser.expected.parenthesis", null, MajicTokenTypes.LEFT_PARENTHESIS);
        int parCount = 0;
        while (builder.getTokenType() != MajicTokenTypes.SEMICOLON) {
            if (builder.getTokenType() == MajicTokenTypes.LEFT_PARENTHESIS) parCount++;
            if (builder.getTokenType() == MajicTokenTypes.RIGHT_PARENTHESIS) {
                if (parCount == 0) builder.error("parser.redundant.parenthesis");
                else parCount--;
            }

            builder.advanceLexer();
        }
        if (parCount > 0) builder.error("parser.expected.parenthesis");
        derivedMarker.done(MajicTokenTypes.ATTRIBUTE_TYPE_ADVANCED);
    }

    /**
     * Parser for advanced part of type definition (the part in curly brackets) for simple data types and SREL
     * @param fromModifyDefinition if true add some extra keywords possible in MODIFY definition
     */
    private void parseAdvancedTypeSimple(MajicPsiBuilder builder, boolean fromModifyDefinition) {
        PsiBuilder.Marker typeAdvancedMarker = builder.mark();
        if (log.isDebugEnabled()) log.debug("parseAdvancedTypeSimple: " + builder.getTokenType());

        while (!builder.eof() && builder.getTokenType() != MajicTokenTypes.RIGHT_CURL_BRACKET) {
            parseAdvancedTypeOneLine(builder, fromModifyDefinition, true);
        }

        if (builder.getTokenType() == MajicTokenTypes.RIGHT_CURL_BRACKET) {
            typeAdvancedMarker.done(MajicTokenTypes.ATTRIBUTE_TYPE_ADVANCED);
            builder.advanceLexer();
        } else {
            builder.error("parser.expected.right_brace");
            typeAdvancedMarker.drop();
        }
    }

    /**
     * Parse one line in Advance Type Simple
     * @param fromModifyDefinition if true add some extra keywords possible in MODIFY definition
     * @param finishWithSemicolon if true semicolon is expected at the end of advanced type definition. This is here
     *                            because MODIFY section must not finish with semicolon, because it is checked in
     *                            MajicParser.parse method
     */
    private void parseAdvancedTypeOneLine(MajicPsiBuilder builder, boolean fromModifyDefinition, boolean finishWithSemicolon) {
        if (log.isDebugEnabled()) log.debug("parseAdvancedTypeOneLine: " + builder.getTokenText());

        if (builder.getTokenType() == MajicTokenTypes.KEYWORD_UI_INFO) {
            builder.advanceLexer(); // eat UI_INFO keyword
            builder.checkAndAdvance("parser.expected.string", null, MajicTokenTypes.STRING);
        } else if (builder.getTokenType() == MajicTokenTypes.KEYWORD_ON_NEW ||
                   builder.getTokenType() == MajicTokenTypes.KEYWORD_ON_DB_INIT) {
            builder.advanceAndCheck("parser.expected.keyword_set", null,
                    MajicTokenTypes.KEYWORD_DEFAULT, MajicTokenTypes.KEYWORD_SET);
            builder.advanceLexer(); // eat DEFAULT keyword
            if (builder.getTokenType() == MajicTokenTypes.INTEGER_NUMBER || builder.getTokenType() == MajicTokenTypes.STRING) {
                builder.advanceLexer();
            } else {
                builder.checkAndAdvance("parser.expected.keyword_user_now", null,
                        MajicTokenTypes.KEYWORD_USER, MajicTokenTypes.KEYWORD_NOW);
            }
        } else if (builder.getTokenType() == MajicTokenTypes.KEYWORD_ON_PRE_VAL ||
                   builder.getTokenType() == MajicTokenTypes.KEYWORD_ON_POST_VAL ||
                   builder.getTokenType() == MajicTokenTypes.KEYWORD_ON_CI ||
                   builder.getTokenType() == MajicTokenTypes.KEYWORD_ATTR_INIT ||
                   builder.getTokenType() == MajicTokenTypes.KEYWORD_ATTR_INIT_VOLATILE) {
            builder.advanceLexer(); // eat ON_PRE_VAL keyword
            if (builder.getTokenType() == MajicTokenTypes.IDENTIFIER) {
                parseMethodDefinition(builder, false);
                builder.checkAndAdvance("parser.expected.integer", null, MajicTokenTypes.INTEGER_NUMBER);
            } else {
                builder.checkAndAdvance("parser.expected.keyword_set", null,
                        MajicTokenTypes.KEYWORD_INCREMENT, MajicTokenTypes.KEYWORD_SET);
                if (builder.getTokenType() == MajicTokenTypes.INTEGER_NUMBER || builder.getTokenType() == MajicTokenTypes.STRING) {
                    builder.advanceLexer();
                } else {
                    builder.checkAndAdvance("parser.expected.keyword_user_now", null,
                            MajicTokenTypes.KEYWORD_USER, MajicTokenTypes.KEYWORD_NOW);
                }
            }
        } else if (fromModifyDefinition && builder.getTokenType() == MajicTokenTypes.KEYWORD_NOT_REQUIRED) {
            builder.advanceLexer();
        } else if (fromModifyDefinition && builder.getTokenType() == MajicTokenTypes.KEYWORD_NOT_WRITE_NEW) {
            builder.advanceLexer();
        } else {
            builder.error("parser.misplaced.keyword", builder.getTokenText());
            builder.advanceLexer();
        }

        if (finishWithSemicolon)
            builder.checkAndAdvance("parser.expected.semicolon", null, MajicTokenTypes.SEMICOLON);
    }

    /**
     * Parser for advanced part of type definition (the part in curly brackets) for BREL and QREL types
     * @param baseType Base type which specify possible keywords (BREL or QREL)
     */
    private void parseAdvancedTypeMultiRel(MajicPsiBuilder b, IElementType baseType) {
        PsiBuilder.Marker typeAdvanceMarker = b.mark();
        
        while (!b.eof() && b.getTokenType() != MajicTokenTypes.RIGHT_CURL_BRACKET) {
            if (log.isDebugEnabled()) log.debug("parseAdvancedTypeMultiRel: " + b.getTokenText() + " (" + baseType.toString() + ")");
            PsiBuilder.Marker advMarkerPart = b.mark();
            if (b.getTokenType() == MajicTokenTypes.KEYWORD_LREL && baseType == MajicTokenTypes.KEYWORD_BREL) {
                b.advanceAndCheck("parser.expected.identifier", null, MajicTokenTypes.IDENTIFIER);
            } else if (b.getTokenType() == MajicTokenTypes.KEYWORD_DOMSET &&
                       (baseType == MajicTokenTypes.KEYWORD_BREL || baseType == MajicTokenTypes.KEYWORD_QREL)) {
                b.advanceAndCheck("parser.expected.identifier", null, MajicTokenTypes.IDENTIFIER);
            } else if (b.getTokenType() == MajicTokenTypes.KEYWORD_MAX_FETCH &&
                        (baseType == MajicTokenTypes.KEYWORD_BREL || baseType == MajicTokenTypes.KEYWORD_QREL)) {
                    b.advanceAndCheck("parser.expected.integer", null, MajicTokenTypes.INTEGER_NUMBER);
            } else if (b.getTokenType() == MajicTokenTypes.KEYWORD_WHERE && baseType == MajicTokenTypes.KEYWORD_QREL) {
                b.advanceAndCheck("parser.expected.string", null, MajicTokenTypes.STRING);
            } else if (b.getTokenType() == MajicTokenTypes.KEYWORD_PARAM_NAMES && baseType == MajicTokenTypes.KEYWORD_QREL) {
                b.advanceAndCheck("parser.expected.left_brace", null, MajicTokenTypes.LEFT_CURL_BRACKET);
                IElementType prevToken = MajicTokenTypes.COLON;
                b.advanceLexer(); // eat {
                while (!b.eof() && b.getTokenType() != MajicTokenTypes.RIGHT_CURL_BRACKET) {
                    if (prevToken == MajicTokenTypes.IDENTIFIER && b.getTokenType() != MajicTokenTypes.COLON) {
                        b.error("parser.expected.colon");
                    } else if (prevToken == MajicTokenTypes.COLON && b.getTokenType() != MajicTokenTypes.IDENTIFIER) {
                        b.error("parser.expected.identifier");
                    }

                    prevToken = b.getTokenType();
                    b.advanceLexer();
                }

                if (prevToken == MajicTokenTypes.COLON) b.error("parser.expected.identifier");
                if (b.eof()) b.error("parser.expected.right_brace");
            } else {
                b.error("parser.misplaced.keyword", b.getTokenText());
                b.advanceLexer();
            }

            b.advanceAndCheck("parser.expected.semicolon", null, MajicTokenTypes.SEMICOLON);
            b.advanceLexer(); // eat semicolon
            advMarkerPart.done(MajicTokenTypes.ATTRIBUTE_TYPE_ADVANCED_PART);
        }

        if (b.getTokenType() == MajicTokenTypes.RIGHT_CURL_BRACKET) {
            typeAdvanceMarker.done(MajicTokenTypes.ATTRIBUTE_TYPE_ADVANCED);
            b.advanceLexer();
        } else {
            b.error("parser.expected.right_brace");
            typeAdvanceMarker.drop();
        }
    }

    /**
     * Parser for main TRIGGERS section in object
     */
    private void parseTriggerSection(MajicPsiBuilder builder) {
        builder.advanceLexer(); // eat TRIGGERS keyword
        final PsiBuilder.Marker triggerSectMarker = builder.mark();
        builder.checkAndAdvance("parser.expected.left_brace", null, MajicTokenTypes.LEFT_CURL_BRACKET, MajicTokenTypes.LEFT_CURL_BRACKET_SS);

        while (!builder.eof() && builder.getTokenType() != MajicTokenTypes.RIGHT_CURL_BRACKET) {
            parseTriggerDefinition(builder);
        }

        if (builder.eof()) {
            builder.error(triggerSectMarker, "parser.expected.right_brace");
            triggerSectMarker.drop();
        } else {
            builder.advanceLexer();
            triggerSectMarker.done(MajicTokenTypes.TRIGGERS_SECTION);
        }
    }

    /**
     * Parse one trigger definition
     */
    private void parseTriggerDefinition(MajicPsiBuilder builder) {
        final PsiBuilder.Marker triggerMarker = builder.mark();
        if (builder.getTokenType() != MajicTokenTypes.KEYWORD_PRE_VALIDATE &&
                builder.getTokenType() != MajicTokenTypes.KEYWORD_POST_VALIDATE &&
                builder.getTokenType() != MajicTokenTypes.KEYWORD_POST_CI) {
            builder.error("parser.expected.trigger_hook");
            triggerMarker.drop();
            builder.advanceLexer(); // eat error token and go to the next
            return;
        }

        builder.advanceLexer();
        parseMethodDefinition(builder, false);

        builder.checkAndAdvance("parser.expected.number", null, MajicTokenTypes.INTEGER_NUMBER);
        builder.checkAndAdvance("parser.expected.keyword_filter", null, MajicTokenTypes.KEYWORD_FILTER);

        // parse FILTER section
        int parCount = 0;
        FilterFsmStatuses fsmStatus = FilterFsmStatuses.OPERAND;
        Stack<PsiBuilder.Marker> markerStack = new Stack<PsiBuilder.Marker>();
        while (!builder.eof() && builder.getTokenType() != MajicTokenTypes.SEMICOLON) {
            if (log.isDebugEnabled()) log.debug(builder.getTokenType().toString() + "; " + builder.getTokenText());
            IElementType tokenType = builder.getTokenType();
            if (tokenType == MajicTokenTypes.LEFT_PARENTHESIS) {
                if (fsmStatus != FilterFsmStatuses.OPERAND) {
                    builder.error("parser.expected.filter_operand");
                    if (!markerStack.empty()) markerStack.pop().drop();
                } else {
                    markerStack.add(builder.mark());
                    fsmStatus = FilterFsmStatuses.OPERAND;
                    parCount++;
                }
            } else if (tokenType == MajicTokenTypes.RIGHT_PARENTHESIS) {
                if (!markerStack.empty()) markerStack.pop().done(MajicTokenTypes.FILTER_EXPRESSION);
                fsmStatus = FilterFsmStatuses.OPERAND;
                parCount--;
            } else if (tokenType == MajicTokenTypes.KEYWORD_EVENT) {
                if (fsmStatus != FilterFsmStatuses.OPERAND) {
                    builder.error("parser.expected.filter_operator");
                    if (!markerStack.empty()) markerStack.pop().drop();
                } else {
                    builder.advanceAndCheck("parser.expected.parenthesis", null, MajicTokenTypes.LEFT_PARENTHESIS);
                    builder.advanceAndCheck("parser.expected.event_spec", null, MajicTokenTypes.STRING);
                    builder.advanceAndCheck("parser.expected.parenthesis", null, MajicTokenTypes.RIGHT_PARENTHESIS);
                    fsmStatus = FilterFsmStatuses.OPERATOR;
                }
            } else if (tokenType == MajicTokenTypes.IDENTIFIER) {
                if (fsmStatus != FilterFsmStatuses.OPERAND) {
                    builder.error("parser.expected.filter_operand");
                    if (!markerStack.empty()) markerStack.pop().drop();
                } else {
                    fsmStatus = FilterFsmStatuses.CONDITION;
                }
            } else if (tokenType == MajicTokenTypes.OP_EQUAL || tokenType == MajicTokenTypes.OP_NOTEQUAL) {
                if (fsmStatus != FilterFsmStatuses.CONDITION) {
                    builder.error((fsmStatus == FilterFsmStatuses.OPERATOR ? "parser.expected.filter_operator" : "parser.expected.filter_operand"));
                    if (!markerStack.empty()) markerStack.pop().drop();
                } else {
                    builder.advanceAndCheck("parser.expected.string_or_int", null,
                            MajicTokenTypes.STRING, MajicTokenTypes.INTEGER_NUMBER);
                    fsmStatus = FilterFsmStatuses.OPERATOR;
                }
            } else if (tokenType == MajicTokenTypes.LEFT_CURL_BRACKET) {
                if (fsmStatus != FilterFsmStatuses.CONDITION) {
                    builder.error((fsmStatus == FilterFsmStatuses.OPERATOR ? "parser.expected.filter_operator" : "parser.expected.filter_operand"));
                    if (!markerStack.empty()) markerStack.pop().drop();
                } else {
                    builder.advanceLexerCondAhead(MajicTokenTypes.INTEGER_NUMBER, MajicTokenTypes.STRING);
                    builder.advanceAndCheck("parser.expected.operator_change", null, MajicTokenTypes.OP_CHANGE);
                    builder.advanceLexerCondAhead(MajicTokenTypes.INTEGER_NUMBER, MajicTokenTypes.STRING);
                    builder.advanceAndCheck("parser.expected.right_brace", null, MajicTokenTypes.RIGHT_CURL_BRACKET);
                    fsmStatus = FilterFsmStatuses.OPERATOR;
                }
            } else if (tokenType == MajicTokenTypes.OP_AND || tokenType == MajicTokenTypes.OP_OR) {
                if (fsmStatus != FilterFsmStatuses.OPERATOR) {
                    builder.error("parser.expected.filter_operator");
                    if (!markerStack.empty()) markerStack.pop().drop();
                } else {
                    fsmStatus = FilterFsmStatuses.OPERAND;
                }

            } else {
                builder.error((fsmStatus == FilterFsmStatuses.OPERATOR ? "parser.expected.filter_operator" : "parser.expected.filter_operand"));
                if (!markerStack.empty()) markerStack.pop().drop();
            }
            builder.advanceLexer();
        }

        if (builder.eof()) {
            builder.error(triggerMarker, "parser.expected.semicolon");
            while (!markerStack.empty()) markerStack.pop().drop();
            triggerMarker.drop();
        } else if (parCount > 0) {
            builder.error("parser.expected.parenthesis");
            while (!markerStack.empty()) markerStack.pop().drop();
            triggerMarker.drop();
        } else {
            builder.advanceLexer(); // eat semicolon
            triggerMarker.done(MajicTokenTypes.TRIGGER_DEFINITION);
        }
    }

    /**
     * Parser for main METHODS section in object
     */
    private void parseMethodsSection(MajicPsiBuilder builder) {
        final PsiBuilder.Marker methodsMarker = builder.mark();
        builder.advanceLexer();  // eat METHODS keyword
        builder.checkAndAdvance("parser.expected.left_brace", null, MajicTokenTypes.LEFT_CURL_BRACKET, MajicTokenTypes.LEFT_CURL_BRACKET_SS);
        while (!builder.eof() && builder.getTokenType() != MajicTokenTypes.RIGHT_CURL_BRACKET) {
            parseMethodDefinition(builder, true);
            builder.checkAndAdvance("parser.expected.semicolon", null, MajicTokenTypes.SEMICOLON);
        }

        if (builder.getTokenType() == MajicTokenTypes.RIGHT_CURL_BRACKET) {
            methodsMarker.done(MajicTokenTypes.METHODS_SECTION);
            builder.advanceLexer();
        } else {
            builder.error("parser.expected.right_brace");
            methodsMarker.drop();
        }
    }
    
    /**
     * Parse method definition (in METHODS section or in trigger definition)
     * @param tridotEnabled If true, this method enable tridot after definition of all attributes
     */
    private void parseMethodDefinition(MajicPsiBuilder builder, boolean tridotEnabled) {
        final PsiBuilder.Marker methodMarker = builder.mark();

        builder.checkAndAdvance("parser.expected.identifier", null, MajicTokenTypes.IDENTIFIER);
        if (log.isDebugEnabled()) log.debug("parseMethodDefinition: " + builder.getTokenType());
        if (builder.getTokenType() == MajicTokenTypes.OP_AT) {
            builder.advanceLexer(); // eat @ sign
            builder.checkAndAdvance("parser.expected.string", null, MajicTokenTypes.STRING);
        }
        builder.checkAndAdvance("parser.expected.parenthesis", null, MajicTokenTypes.LEFT_PARENTHESIS);

        IElementType prevType = MajicTokenTypes.COLON;
        // stay true if method is without parameters
        boolean isEmpty = true;
        while (builder.getTokenType() == MajicTokenTypes.IDENTIFIER ||
                builder.getTokenType() == MajicTokenTypes.COLON ||
                builder.getTokenType() == MajicTokenTypes.STRING ||
                builder.getTokenType() == MajicTokenTypes.INTEGER_NUMBER ||
                builder.getTokenType() == MajicTokenTypes.PIPE) {
            isEmpty = false;
            if (prevType == MajicTokenTypes.COLON) {
                if (builder.getTokenType() == MajicTokenTypes.COLON) {
                    builder.error(SpellIdeBundle.message("parser.expected.identifier"));
                    methodMarker.drop();
                    return;
                } else {
                    prevType = MajicTokenTypes.IDENTIFIER;
                    builder.advanceLexer();
                }
            } else if ( builder.getTokenType() == MajicTokenTypes.PIPE) {
                builder.advanceLexer();
                if (!builder.getTokenText().equals("nil")) {
                    builder.error("parser.expected.nil");
                }
                builder.advanceAndCheck("parser.expected.colon", null, MajicTokenTypes.COLON);
                prevType = MajicTokenTypes.COLON;
                builder.advanceLexer();
            } else if ( builder.getTokenType() != MajicTokenTypes.COLON) {
                builder.error(SpellIdeBundle.message("parser.expected.colon"));
                methodMarker.drop();
                return;
            } else {
                prevType = MajicTokenTypes.COLON;
                builder.advanceLexer();
            }
        }

        if (builder.getTokenType() == MajicTokenTypes.OP_TRIDOT) {
           if (!tridotEnabled || prevType != MajicTokenTypes.COLON) {
               builder.error(SpellIdeBundle.message("parser.expected.parenthesis"));
               methodMarker.drop();
               return;
           } else {
               prevType = MajicTokenTypes.IDENTIFIER;
               builder.advanceLexer();
           }
        }

        if (builder.getTokenType() != MajicTokenTypes.RIGHT_PARENTHESIS ||
            (!isEmpty && prevType != MajicTokenTypes.IDENTIFIER)) {
            builder.error(SpellIdeBundle.message("parser.expected.parenthesis"));
            methodMarker.drop();
            return;
        } else {
            builder.advanceLexer();
        }

        methodMarker.done(MajicTokenTypes.IDENTIFIER);
    }

    /**
     * Parser for main FACTORY section in object
     */
    private void parseFactorySection(MajicPsiBuilder builder) {
        if (log.isDebugEnabled()) log.debug("parseFactorySection: " + builder.getTokenType().toString());
        builder.advanceLexer();  // eat FACTORY keyword
        builder.advanceLexerCond(MajicTokenTypes.IDENTIFIER);  // eat factory name
        final PsiBuilder.Marker factoryMarker = builder.mark();

        builder.checkAndAdvance("parser.expected.left_brace", null, MajicTokenTypes.LEFT_CURL_BRACKET, MajicTokenTypes.LEFT_CURL_BRACKET_SS);
        while (!builder.eof() && builder.getTokenType() != MajicTokenTypes.RIGHT_CURL_BRACKET) {
            if (log.isDebugEnabled()) log.debug("parseFactorySectionInner: " + builder.getTokenType().toString());
            if (builder.getTokenType() == MajicTokenTypes.KEYWORD_METHODS) {
                parseMethodsSection(builder);
            } else if (builder.getTokenType() == MajicTokenTypes.KEYWORD_STANDARD_LISTS) {
                parseStandardListsSection(builder);
            } else if (builder.getTokenType() == MajicTokenTypes.KEYWORD_DOMSET) {
                builder.advanceLexer();
                builder.checkAndAdvance("parser.expected.identifier", null, MajicTokenTypes.IDENTIFIER);
                builder.checkAndAdvance("parser.expected.string", null, MajicTokenTypes.STRING);
                builder.checkAndAdvance("parser.expected.string", null, MajicTokenTypes.STRING);
                builder.checkAndAdvance("parser.expected.keyword_static_dynamic", null, MajicTokenTypes.KEYWORD_DYNAMIC, MajicTokenTypes.KEYWORD_STATIC);
            } else if (builder.getTokenType() == MajicTokenTypes.KEYWORD_REL_ATTR) {
                builder.advanceLexer();
                builder.checkAndAdvance("parser.expected.identifier", null, MajicTokenTypes.IDENTIFIER);
            } else if (builder.getTokenType() == MajicTokenTypes.KEYWORD_COMMON_NAME) {
                builder.advanceLexer();
                builder.checkAndAdvance("parser.expected.identifier", null, MajicTokenTypes.IDENTIFIER);
            } else if (builder.getTokenType() == MajicTokenTypes.KEYWORD_FUNCTION_GROUP) {
                builder.advanceLexer();
                builder.checkAndAdvance("parser.expected.string", null, MajicTokenTypes.STRING);
            } else if (builder.getTokenType() == MajicTokenTypes.KEYWORD_RESTRICT) {
                builder.advanceLexer();
                builder.checkAndAdvance("parser.expected.string", null, MajicTokenTypes.STRING);
                builder.checkAndAdvance("parser.expected.left_brace", null, MajicTokenTypes.LEFT_CURL_BRACKET);
                boolean firstCond = true;
                while (!builder.eof() && builder.getTokenType() != MajicTokenTypes.RIGHT_CURL_BRACKET) {
                    parseRestrictCondition(builder, firstCond);
                    firstCond = false;
                }

                if (builder.getTokenType() == MajicTokenTypes.RIGHT_CURL_BRACKET) {
                    builder.advanceLexer();
                } else {
                    builder.error("parser.expected.right_brace");
                }
            } else {
                builder.error("parser.misplaced.keyword", builder.getTokenText());
                builder.advanceLexer();
            }

            builder.checkAndAdvance("parser.expected.semicolon", null, MajicTokenTypes.SEMICOLON);
        }

        if (builder.getTokenType() == MajicTokenTypes.RIGHT_CURL_BRACKET) {
            factoryMarker.done(MajicTokenTypes.FACTORY_SECTION);
            builder.advanceLexer();
        } else {
            builder.error("parser.expected.right_brace");
            factoryMarker.drop();
        }
    }

    /**
     * Parser for STANDARD_LISTS section in FACTORY
     */
    private void parseStandardListsSection(MajicPsiBuilder builder) {
        if (log.isDebugEnabled()) log.debug("parseStandardListSection: " + builder.getTokenType().toString());

        final PsiBuilder.Marker stdListsMarker = builder.mark();
        builder.advanceLexer(); // eat STANDARD_LISTS keyword
        builder.checkAndAdvance("parser.expected.left_brace", null, MajicTokenTypes.LEFT_CURL_BRACKET, MajicTokenTypes.LEFT_CURL_BRACKET_SS);
        while (!builder.eof() && builder.getTokenType() != MajicTokenTypes.RIGHT_CURL_BRACKET) {
            if (log.isDebugEnabled()) log.debug("parseStandardListSectionInner: " + builder.getTokenType().toString());
            if (builder.getTokenType() == MajicTokenTypes.KEYWORD_WHERE) {
                builder.advanceLexer(); // eat WHERE keyword
                builder.checkAndAdvance("parser.expected.string", null, MajicTokenTypes.STRING);
            } else if (builder.getTokenType() == MajicTokenTypes.KEYWORD_SORT_BY) {
                builder.advanceLexer(); // eat SORT_BY keyword
                builder.checkAndAdvance("parser.expected.string", null, MajicTokenTypes.STRING);
            } else if (builder.getTokenType() == MajicTokenTypes.KEYWORD_MLIST || 
                       builder.getTokenType() == MajicTokenTypes.KEYWORD_RLIST) {
                builder.advanceLexer(); // eat MLIST and RLIST keyword
                builder.checkAndAdvance("parser.expected.keyword_on_of", null, MajicTokenTypes.KEYWORD_OFF, MajicTokenTypes.KEYWORD_ON);
            } else if (builder.getTokenType() == MajicTokenTypes.KEYWORD_FETCH) {
                builder.advanceLexer(); // eat FETCH keyword
                builder.checkAndAdvance("parser.expected.string", null, MajicTokenTypes.STRING);
            } else {
                builder.error("parser.misplaced.keyword", builder.getTokenText());
                builder.advanceLexer();
            }

            builder.checkAndAdvance("parser.expected.semicolon", null, MajicTokenTypes.SEMICOLON);
        }

        if (builder.getTokenType() == MajicTokenTypes.RIGHT_CURL_BRACKET) {
            stdListsMarker.done(MajicTokenTypes.STANDARD_LISTS_SECTION);
            builder.advanceLexer();
        } else {
            builder.error("parser.expected.right_brace");
            stdListsMarker.drop();
        }
    }

    private void parseRestrictCondition(MajicPsiBuilder builder, boolean firstCond) {
        if (!firstCond) builder.checkAndAdvance("parser.expected.colon", null, MajicTokenTypes.COLON);
        builder.checkAndAdvance("parser.expected.identifier", null, MajicTokenTypes.IDENTIFIER);
        builder.checkAndAdvance("parser.expected.restrict_equal", null, MajicTokenTypes.OP_ASSIGN, MajicTokenTypes.OP_NOTEQUAL);
        builder.checkAndAdvance("parser.expected.string_or_int", null, MajicTokenTypes.INTEGER_NUMBER, MajicTokenTypes.STRING);
    }


    enum FilterFsmStatuses {
        OPERAND,
        OPERATOR,
        CONDITION
    }
}
