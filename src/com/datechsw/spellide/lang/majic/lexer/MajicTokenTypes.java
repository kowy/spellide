package com.datechsw.spellide.lang.majic.lexer;

import com.datechsw.spellide.lang.majic.MajicLanguage;
import com.intellij.psi.TokenType;
import com.intellij.psi.tree.IElementType;
import com.intellij.psi.tree.IFileElementType;
import com.intellij.psi.tree.IStubFileElementType;
import com.intellij.psi.tree.TokenSet;

/**
 * Interface providing all Tokens returned by Majic lexer
 */
public interface MajicTokenTypes {
    MajicLanguage LANG = MajicLanguage.INSTANCE;

    IFileElementType FILE = new IStubFileElementType(LANG);

    IElementType WHITE_SPACE = TokenType.WHITE_SPACE;
    IElementType BAD_CHARACTER = TokenType.BAD_CHARACTER;

    IElementType COMMENT = new MajicElementType("COMMENT");
    IElementType IDENTIFIER = new MajicElementType("IDENTIFIER");
    IElementType STRING = new MajicElementType("STRING");
    IElementType INTEGER_NUMBER = new MajicElementType("INTEGER_NUMBER");
    
    // braces
    IElementType LEFT_CURL_BRACKET = new MajicElementType("LEFT_CURL_BRACKET");
    // only for Lexer to distinguis between Status Switch braces and others
    IElementType LEFT_CURL_BRACKET_SS = new MajicElementType("LEFT_CURL_BRACKET_SS");
    IElementType RIGHT_CURL_BRACKET = new MajicElementType("RIGHT_CURL_BRACKET");
    IElementType LEFT_PARENTHESIS = new MajicElementType("LEFT_PARENTHESIS");
    IElementType RIGHT_PARENTHESIS = new MajicElementType("RIGHT_PARENTHESIS");

    // keywords
    IElementType KEYWORD_OBJECT = new MajicElementType("KEYWORD_OBJECT");
    IElementType KEYWORD_LREL = new MajicElementType("KEYWORD_LREL");
    IElementType KEYWORD_PDM = new MajicElementType("KEYWORD_PDM");
    IElementType KEYWORD_TENANT_OPTIONAL = new MajicElementType("KEYWORD_TENANT_OPTIONAL");
    IElementType KEYWORD_TENANT_REQUIRED = new MajicElementType("KEYWORD_TENANT_REQUIRED");
    IElementType KEYWORD_MODIFY = new MajicElementType("KEYWORD_MODIFY");
    IElementType KEYWORD_ATTRIBUTES = new MajicElementType("KEYWORD_ATTRIBUTES");
    IElementType KEYWORD_SECONDARY = new MajicElementType("KEYWORD_SECONDARY");
    IElementType KEYWORD_OWNER_ATTR = new MajicElementType("KEYWORD_OWNER_ATTR");
    IElementType KEYWORD_TRIGGERS = new MajicElementType("KEYWORD_TRIGGERS");
    IElementType KEYWORD_METHODS = new MajicElementType("KEYWORD_METHODS");
    IElementType KEYWORD_FACTORY = new MajicElementType("KEYWORD_FACTORY");
    IElementType KEYWORD_NEW_INIT = new MajicElementType("KEYWORD_NEW_INIT");
    IElementType KEYWORD_PRE_VALIDATE = new MajicElementType("KEYWORD_PRE_VALIDATE");
    IElementType KEYWORD_POST_VALIDATE = new MajicElementType("KEYWORD_POST_VALIDATE");
    IElementType KEYWORD_POST_CI = new MajicElementType("KEYWORD_POST_CI");
    IElementType KEYWORD_FILTER = new MajicElementType("KEYWORD_FILTER");
    IElementType KEYWORD_EVENT = new MajicElementType("KEYWORD_EVENT");
    IElementType KEYWORD_REQUIRED = new MajicElementType("KEYWORD_REQUIRED");
    IElementType KEYWORD_NOT_REQUIRED = new MajicElementType("KEYWORD_NOT_REQUIRED");
    IElementType KEYWORD_NOT_WRITE_NEW = new MajicElementType("KEYWORD_NOT_WRITE_NEW");
    IElementType KEYWORD_ON_DB_INIT = new MajicElementType("KEYWORD_ON_DB_INIT");
    IElementType KEYWORD_ON_NEW = new MajicElementType("KEYWORD_ON_NEW");
    IElementType KEYWORD_ON_CI = new MajicElementType("KEYWORD_ON_CI");
    IElementType KEYWORD_ON_PRE_VAL = new MajicElementType("KEYWORD_ON_PRE_VAL");
    IElementType KEYWORD_ON_POST_VAL = new MajicElementType("KEYWORD_ON_POST_VAL");
    IElementType KEYWORD_ATTR_INIT = new MajicElementType("KEYWORD_ATTR_INIT");
    IElementType KEYWORD_ATTR_INIT_VOLATILE = new MajicElementType("KEYWORD_ATTR_INIT_VOLATILE");
    IElementType KEYWORD_DEFAULT = new MajicElementType("KEYWORD_DEFAULT");
    IElementType KEYWORD_SET = new MajicElementType("KEYWORD_SET");
    IElementType KEYWORD_INCREMENT = new MajicElementType("KEYWORD_INCREMENT");
    IElementType KEYWORD_USER = new MajicElementType("KEYWORD_USER");
    IElementType KEYWORD_NOW = new MajicElementType("KEYWORD_NOW");
    IElementType KEYWORD_RESTRICT = new MajicElementType("KEYWORD_RESTRICT");
    IElementType KEYWORD_PARAM_NAMES = new MajicElementType("KEYWORD_PARAM_NAMES");
    IElementType KEYWORD_FUNCTION_GROUP = new MajicElementType("KEYWORD_FUNCTION_GROUP");
    IElementType KEYWORD_UI_INFO = new MajicElementType("KEYWORD_UI_INFO");
    IElementType KEYWORD_REL_ATTR = new MajicElementType("KEYWORD_REL_ATTR");
    IElementType KEYWORD_COMMON_NAME = new MajicElementType("KEYWORD_COMMON_NAME");
    IElementType KEYWORD_DOMSET = new MajicElementType("KEYWORD_DOMSET");
    IElementType KEYWORD_STANDARD_LISTS = new MajicElementType("KEYWORD_STANDARD_LISTS");
    IElementType KEYWORD_SORT_BY = new MajicElementType("KEYWORD_SORT_BY");
    IElementType KEYWORD_FETCH = new MajicElementType("KEYWORD_FETCH");
    IElementType KEYWORD_MAX_FETCH = new MajicElementType("KEYWORD_MAX_FETCH");
    IElementType KEYWORD_WHERE = new MajicElementType("KEYWORD_WHERE");
    IElementType KEYWORD_MLIST = new MajicElementType("KEYWORD_MLIST");
    IElementType KEYWORD_RLIST = new MajicElementType("KEYWORD_RLIST");
    IElementType KEYWORD_STATIC = new MajicElementType("KEYWORD_STATIC");
    IElementType KEYWORD_DYNAMIC = new MajicElementType("KEYWORD_DYNAMIC");
    IElementType KEYWORD_OFF = new MajicElementType("KEYWORD_OFF");
    IElementType KEYWORD_ON = new MajicElementType("KEYWORD_ON");
    IElementType KEYWORD_DISTINCT = new MajicElementType("KEYWORD_DISTINCT");
    IElementType KEYWORD_UUID = new MajicElementType("KEYWORD_UUID");
    IElementType KEYWORD_STRING = new MajicElementType("KEYWORD_STRING");
    IElementType KEYWORD_INTEGER = new MajicElementType("KEYWORD_INTEGER");
    IElementType KEYWORD_DATE = new MajicElementType("KEYWORD_DATE");
    IElementType KEYWORD_DURATION = new MajicElementType("KEYWORD_DURATION");
    IElementType KEYWORD_SREL = new MajicElementType("KEYWORD_SREL");
    IElementType KEYWORD_BREL = new MajicElementType("KEYWORD_BREL");
    IElementType KEYWORD_QREL = new MajicElementType("KEYWORD_QREL");
    IElementType KEYWORD_DERIVED = new MajicElementType("KEYWORD_DERIVED");
    IElementType KEYWORD_LOCAL = new MajicElementType("KEYWORD_LOCAL");
    IElementType KEYWORD_DISPLAY_NAME = new MajicElementType("KEYWORD_DISPLAY_NAME");
    IElementType KEYWORD_DISPLAY_GROUP = new MajicElementType("KEYWORD_DISPLAY_GROUP");
    IElementType KEYWORD_SERVICE_PROVIDER_ELIGIBLE = new MajicElementType("KEYWORD_SERVICE_PROVIDER_ELIGIBLE");

    // operators
    IElementType SEMICOLON = new MajicElementType("SEMICOLON");
    IElementType COLON = new MajicElementType("COLON");
    IElementType PIPE = new MajicElementType("PIPE");
    IElementType OP_AT = new MajicElementType("OP_AT");
    IElementType OP_ASSIGN = new MajicElementType("OP_ASSIGN");
    IElementType OP_TRIDOT = new MajicElementType("OP_TRIDOT");
    IElementType OP_CHANGE = new MajicElementType("OP_CHANGE");
    IElementType OP_EQUAL = new MajicElementType("OP_EQUAL");
    IElementType OP_NOTEQUAL = new MajicElementType("OP_NOTEQUAL");
    IElementType OP_AND = new MajicElementType("OP_AND");
    IElementType OP_OR = new MajicElementType("OP_OR");

    ////////////// Composite elements
    IElementType OBJECT_DEFINITION = new MajicElementType("OBJECT_DEFINITION");
    IElementType MODIFY_DEFINITION = new MajicElementType("MODIFY_DEFINITION");
    IElementType ATTRIBUTES_SECTION = new MajicElementType("ATTRIBUTES_SECTION");
    IElementType ATTRIBUTE_DEFINITION = new MajicElementType("ATTRIBUTE_DEFINITION");
    IElementType ATTRIBUTE_TYPE = new MajicElementType("ATTRIBUTE_TYPE");
    IElementType ATTRIBUTE_TYPE_ADVANCED = new MajicElementType("ATTRIBUTE_TYPE_ADVANCED");
    IElementType ATTRIBUTE_TYPE_ADVANCED_PART = new MajicElementType("ATTRIBUTE_TYPE_ADVANCED_PART");
    IElementType TRIGGER_DEFINITION = new MajicElementType("TRIGGER_DEFINITION");
    IElementType TRIGGERS_SECTION = new MajicElementType("TRIGGERS_SECTION");
    IElementType FILTER_EXPRESSION = new MajicElementType("FILTER_EXPRESSION");
    IElementType METHODS_SECTION = new MajicElementType("METHODS_SECTION");
    IElementType FACTORY_SECTION = new MajicElementType("FACTORY_SECTION");
    IElementType STANDARD_LISTS_SECTION = new MajicElementType("STANDARD_LISTS_SECTION");

    ////////////// Token Sets
    TokenSet WHITESPACES = TokenSet.create(WHITE_SPACE);
    TokenSet COMMENTS = TokenSet.create(COMMENT);

    TokenSet PARENTHESIS = TokenSet.create(LEFT_PARENTHESIS, RIGHT_PARENTHESIS);
    TokenSet CURL_BRACKETS = TokenSet.create(LEFT_CURL_BRACKET, RIGHT_CURL_BRACKET, LEFT_CURL_BRACKET_SS);

    TokenSet NUMBERS = TokenSet.create(INTEGER_NUMBER);
    TokenSet OPERATORS = TokenSet.create(SEMICOLON, COLON, OP_TRIDOT, PIPE, OP_AT, OP_ASSIGN, OP_CHANGE, OP_EQUAL);
    TokenSet KEYWORDS = TokenSet.create(KEYWORD_OBJECT, KEYWORD_LREL, KEYWORD_PDM, KEYWORD_TENANT_OPTIONAL,
            KEYWORD_TENANT_REQUIRED, KEYWORD_MODIFY, KEYWORD_ATTRIBUTES, KEYWORD_SECONDARY, KEYWORD_OWNER_ATTR,
            KEYWORD_TRIGGERS, KEYWORD_METHODS, KEYWORD_FACTORY, KEYWORD_NEW_INIT, KEYWORD_PRE_VALIDATE,
            KEYWORD_POST_VALIDATE, KEYWORD_POST_CI, KEYWORD_FILTER, KEYWORD_EVENT, KEYWORD_REQUIRED, KEYWORD_NOT_REQUIRED,
            KEYWORD_NOT_WRITE_NEW, KEYWORD_ON_DB_INIT, KEYWORD_ON_NEW, KEYWORD_ON_CI, KEYWORD_ON_PRE_VAL,
            KEYWORD_ON_POST_VAL, KEYWORD_ATTR_INIT, KEYWORD_ATTR_INIT_VOLATILE, KEYWORD_DEFAULT, KEYWORD_SET,
            KEYWORD_INCREMENT, KEYWORD_USER, KEYWORD_NOW, KEYWORD_RESTRICT, KEYWORD_PARAM_NAMES, KEYWORD_FUNCTION_GROUP,
            KEYWORD_UI_INFO, KEYWORD_REL_ATTR, KEYWORD_COMMON_NAME, KEYWORD_DOMSET, KEYWORD_STANDARD_LISTS,
            KEYWORD_SORT_BY, KEYWORD_FETCH, KEYWORD_MAX_FETCH, KEYWORD_WHERE, KEYWORD_MLIST, KEYWORD_RLIST,
            KEYWORD_STATIC, KEYWORD_DYNAMIC, KEYWORD_OFF, KEYWORD_DISTINCT, KEYWORD_UUID, KEYWORD_STRING, KEYWORD_INTEGER,
            KEYWORD_DATE, KEYWORD_DURATION, KEYWORD_SREL, KEYWORD_BREL, KEYWORD_QREL, KEYWORD_DERIVED, KEYWORD_LOCAL,
            KEYWORD_DISPLAY_NAME, KEYWORD_DISPLAY_GROUP, KEYWORD_SERVICE_PROVIDER_ELIGIBLE);
}
