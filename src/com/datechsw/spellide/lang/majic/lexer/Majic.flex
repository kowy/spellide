package com.datechsw.spellide.lang.majic.lexer;

import java.util.*;
import com.intellij.lexer.FlexLexer;
import com.intellij.psi.tree.IElementType;

%%
%class _MajicLexer
%implements FlexLexer
%unicode
/* change the name of the function next_token - this is necessary for compatibility with Flex implementation in IntelliJ */
%function advance
/* change the type of return value of the function next_token - this is necessary for compatibility with Flex implementation in IntelliJ */
%type IElementType
/* what should be done when input file EOF is reached */
%eof{ return;
%eof}

%{
  private Stack<IElementType> braceCounter = new Stack <IElementType>();
  private Stack<Integer> statusStack = new Stack<Integer>();
  private boolean waitForStatusSwitch = false;
%}

/* definice maker - tabulka typických terminálů ve zdrojovém souboru */
CRLF = \n | \r | \r\n
WHITE_SPACE = [ \t]
WHITE_SPACE_EOL = [ \n\r\t\f]
NON_EOL_CHARACTER = [^\n\r\f]
IDENTIFIER = [:jletter:][:jletterdigit:]*
NON_WHITE_SPACE = [^\ \t]

ONE_LINE_COMMENT = {WHITE_SPACE}* "//" .*
MULTI_LINE_COMMENT = "/*" [^*] ~"*/" | "/*" "*"+ "/"
DOCUMENTATION_COMMENT = "/**" {COMMENT_CONTENT} "*"+ "/"
COMMENT_CONTENT = ( [^*] | "*"+ [^*/] )*
COMMENT = {ONE_LINE_COMMENT} | {MULTI_LINE_COMMENT} | {DOCUMENTATION_COMMENT}

///// String literals
STRING_LITERAL=\"([^\\\"\r\n]|{ESCAPE_SEQUENCE})*(\"|\\)?
ESCAPE_SEQUENCE="\\"[^ \r\n]

///// Integers and floats
INTEGER_LITERAL = [-]?[:digit:]+

/* status definition - other non-standard non-terminals in a pushdown automaton */
%state OBJECT_STATEMENT
%state ATTRIBUTES_DEFINITION, TRIGGERS_DEFINITION, FACTORY_DEFINITION, METHODS_DEFINITION
%%

<YYINITIAL> {
    {WHITE_SPACE_EOL}+          { return MajicTokenTypes.WHITE_SPACE; }
    {ONE_LINE_COMMENT}          { return MajicTokenTypes.COMMENT; }
    {MULTI_LINE_COMMENT}        { return MajicTokenTypes.COMMENT; }

    "OBJECT"                    { yybegin(OBJECT_STATEMENT); return MajicTokenTypes.KEYWORD_OBJECT; }
    "MODIFY"                    { statusStack.push(YYINITIAL); yybegin(ATTRIBUTES_DEFINITION); return MajicTokenTypes.KEYWORD_MODIFY; }

    ";"                         { return MajicTokenTypes.SEMICOLON; }

    {STRING_LITERAL}            { return MajicTokenTypes.STRING; }

    {NON_WHITE_SPACE}           { return MajicTokenTypes.BAD_CHARACTER; }
}

<OBJECT_STATEMENT> {
    {ONE_LINE_COMMENT}          { return MajicTokenTypes.COMMENT; }
    {MULTI_LINE_COMMENT}        { return MajicTokenTypes.COMMENT; }
    
    "LREL"                      { return MajicTokenTypes.KEYWORD_LREL; }
    "PDM"                       { return MajicTokenTypes.KEYWORD_PDM; }
    "LOCAL"                     { return MajicTokenTypes.KEYWORD_LOCAL; }
    "TENANT_OPTIONAL"           { return MajicTokenTypes.KEYWORD_TENANT_OPTIONAL; }
    "TENANT_REQUIRED"           { return MajicTokenTypes.KEYWORD_TENANT_REQUIRED; }
    "DISPLAY_NAME"              { return MajicTokenTypes.KEYWORD_DISPLAY_NAME; }
    "DISPLAY_GROUP"             { return MajicTokenTypes.KEYWORD_DISPLAY_GROUP; }

    "ATTRIBUTES"                {
                                  statusStack.push(OBJECT_STATEMENT);
                                  yybegin(ATTRIBUTES_DEFINITION);
                                  waitForStatusSwitch = true;
                                  return MajicTokenTypes.KEYWORD_ATTRIBUTES;
                                }
    "FACTORY"                   {
                                  statusStack.push(OBJECT_STATEMENT);
                                  yybegin(FACTORY_DEFINITION);
                                  waitForStatusSwitch = true;
                                  return MajicTokenTypes.KEYWORD_FACTORY;
                                }
    "TRIGGERS"                  {
                                  statusStack.push(OBJECT_STATEMENT);
                                  yybegin(TRIGGERS_DEFINITION);
                                  waitForStatusSwitch = true;
                                  return MajicTokenTypes.KEYWORD_TRIGGERS;
                                }
    "METHODS"                   { statusStack.push(OBJECT_STATEMENT); yybegin(METHODS_DEFINITION); return MajicTokenTypes.KEYWORD_METHODS; }

    ";"                         { return MajicTokenTypes.SEMICOLON; }

    ////////////// braces ////////////////
    "{"                         { braceCounter.push(MajicTokenTypes.LEFT_CURL_BRACKET); return MajicTokenTypes.LEFT_CURL_BRACKET; }
    "}"                         {
                                   if (!braceCounter.isEmpty() && MajicTokenTypes.LEFT_CURL_BRACKET == braceCounter.peek()) {
                                       braceCounter.pop();
                                       if (braceCounter.isEmpty()) yybegin(YYINITIAL);
                                       return MajicTokenTypes.RIGHT_CURL_BRACKET;
                                   } else {
                                       return MajicTokenTypes.BAD_CHARACTER;
                                   }
                                }


    {IDENTIFIER}                { return MajicTokenTypes.IDENTIFIER; }
    {STRING_LITERAL}            { return MajicTokenTypes.STRING; }
    {WHITE_SPACE_EOL}+          { return MajicTokenTypes.WHITE_SPACE; }

    {NON_WHITE_SPACE}           { return MajicTokenTypes.BAD_CHARACTER; }
}

<ATTRIBUTES_DEFINITION> {
    {ONE_LINE_COMMENT}          { return MajicTokenTypes.COMMENT; }
    {MULTI_LINE_COMMENT}        { return MajicTokenTypes.COMMENT; }
    "UUID"                      { return MajicTokenTypes.KEYWORD_UUID; }
    "STRING"                    { return MajicTokenTypes.KEYWORD_STRING; }
    "INTEGER"                   { return MajicTokenTypes.KEYWORD_INTEGER; }
    "DATE"                      { return MajicTokenTypes.KEYWORD_DATE; }
    "DURATION"                  { return MajicTokenTypes.KEYWORD_DURATION; }
    "SREL"                      { return MajicTokenTypes.KEYWORD_SREL; }
    "BREL"                      { statusStack.push(ATTRIBUTES_DEFINITION); waitForStatusSwitch = true; yybegin(FACTORY_DEFINITION);return MajicTokenTypes.KEYWORD_BREL; }
    "QREL"                      { statusStack.push(ATTRIBUTES_DEFINITION); waitForStatusSwitch = true; yybegin(FACTORY_DEFINITION);return MajicTokenTypes.KEYWORD_QREL; }
    "DERIVED"                   { return MajicTokenTypes.KEYWORD_DERIVED; }
    "LOCAL"                     { return MajicTokenTypes.KEYWORD_LOCAL; }
    "DISPLAY_NAME"              { return MajicTokenTypes.KEYWORD_DISPLAY_NAME; }
    "ATTR_INIT"                 { return MajicTokenTypes.KEYWORD_ATTR_INIT; }
    "ATTR_INIT_VOLATILE"        { return MajicTokenTypes.KEYWORD_ATTR_INIT_VOLATILE; }
    "ON_DB_INIT"                { return MajicTokenTypes.KEYWORD_ON_DB_INIT; }
    "ON_NEW"                    { return MajicTokenTypes.KEYWORD_ON_NEW; }
    "ON_CI"                     { return MajicTokenTypes.KEYWORD_ON_CI; }
    "ON_PRE_VAL"                { return MajicTokenTypes.KEYWORD_ON_PRE_VAL; }
    "ON_POST_VAL"               { return MajicTokenTypes.KEYWORD_ON_POST_VAL; }
    "UI_INFO"                   { return MajicTokenTypes.KEYWORD_UI_INFO; }
    "DEFAULT"                   { return MajicTokenTypes.KEYWORD_DEFAULT; }
    "SET"                       { return MajicTokenTypes.KEYWORD_SET; }
    "INCREMENT"                 { return MajicTokenTypes.KEYWORD_INCREMENT; }
    "USER"                      { return MajicTokenTypes.KEYWORD_USER; }
    "NOW"                       { return MajicTokenTypes.KEYWORD_NOW; }
    "REQUIRED"                  { return MajicTokenTypes.KEYWORD_REQUIRED; }
    "NOT_REQUIRED"              { return MajicTokenTypes.KEYWORD_NOT_REQUIRED; }
    "NOT_WRITE_NEW"             { return MajicTokenTypes.KEYWORD_NOT_WRITE_NEW; }
    "SERVICE_PROVIDER_ELIGIBLE" { return MajicTokenTypes.KEYWORD_SERVICE_PROVIDER_ELIGIBLE; }
    "SECONDARY"                 { return MajicTokenTypes.KEYWORD_SECONDARY; }
    "OWNER_ATTR"                { return MajicTokenTypes.KEYWORD_OWNER_ATTR; }

    ";"                         {
                                  // MODIFY section may be withou braces (so first semicolon ends ATTRIBUTES_DEFINITION)
                                  if (braceCounter.isEmpty()) yybegin(statusStack.pop());
                                  return MajicTokenTypes.SEMICOLON;
                                }
    ","                         { return MajicTokenTypes.COLON; }
    "@"                         { return MajicTokenTypes.OP_AT; }

    ////////////// braces ////////////////
    "{"                         { if (waitForStatusSwitch) {
                                    braceCounter.push(MajicTokenTypes.LEFT_CURL_BRACKET_SS);
                                    waitForStatusSwitch = false;
                                  } else {
                                    braceCounter.push(MajicTokenTypes.LEFT_CURL_BRACKET);
                                  }
                                  return MajicTokenTypes.LEFT_CURL_BRACKET;
                                }
    "}"                         {
                                   if (!braceCounter.isEmpty()) {
                                       if (MajicTokenTypes.LEFT_CURL_BRACKET_SS == braceCounter.peek() && !statusStack.isEmpty()) yybegin(statusStack.pop());
                                       braceCounter.pop();
                                       return MajicTokenTypes.RIGHT_CURL_BRACKET;
                                   } else {
                                       return MajicTokenTypes.BAD_CHARACTER;
                                   }
                                }
    "("                         { braceCounter.push(MajicTokenTypes.LEFT_PARENTHESIS); return MajicTokenTypes.LEFT_PARENTHESIS; }
    ")"                         {
                                    if (!braceCounter.isEmpty() && MajicTokenTypes.LEFT_PARENTHESIS == braceCounter.peek()) {
                                        braceCounter.pop(); return MajicTokenTypes.RIGHT_PARENTHESIS;
                                    } else {
                                        return MajicTokenTypes.BAD_CHARACTER;
                                    }
                                }



    {INTEGER_LITERAL}           { return MajicTokenTypes.INTEGER_NUMBER; }
    {IDENTIFIER}                { return MajicTokenTypes.IDENTIFIER; }
    {STRING_LITERAL}            { return MajicTokenTypes.STRING; }
    {WHITE_SPACE_EOL}+          { return MajicTokenTypes.WHITE_SPACE; }

    {NON_WHITE_SPACE}           { return MajicTokenTypes.BAD_CHARACTER; }
}

<TRIGGERS_DEFINITION> {
    {ONE_LINE_COMMENT}          { return MajicTokenTypes.COMMENT; }
    {MULTI_LINE_COMMENT}        { return MajicTokenTypes.COMMENT; }
    "NEW_INIT"                  { return MajicTokenTypes.KEYWORD_NEW_INIT; }
    "PRE_VALIDATE"              { return MajicTokenTypes.KEYWORD_PRE_VALIDATE; }
    "POST_VALIDATE"             { return MajicTokenTypes.KEYWORD_POST_VALIDATE; }
    "POST_CI"                   { return MajicTokenTypes.KEYWORD_POST_CI; }
    "FILTER"                    { return MajicTokenTypes.KEYWORD_FILTER; }
    "EVENT"                     { return MajicTokenTypes.KEYWORD_EVENT; }

    ";"                         { return MajicTokenTypes.SEMICOLON; }
    ","                         { return MajicTokenTypes.COLON; }
    "->"                        { return MajicTokenTypes.OP_CHANGE; }
    "=="                        { return MajicTokenTypes.OP_EQUAL; }
    "!="                        { return MajicTokenTypes.OP_NOTEQUAL; }
    "&&"                        { return MajicTokenTypes.OP_AND; }
    "||"                        { return MajicTokenTypes.OP_OR; }

    ////////////// braces ////////////////
    "{"                         { if (waitForStatusSwitch) {
                                    braceCounter.push(MajicTokenTypes.LEFT_CURL_BRACKET_SS);
                                    waitForStatusSwitch = false;
                                  } else {
                                    braceCounter.push(MajicTokenTypes.LEFT_CURL_BRACKET);
                                  }
                                  return MajicTokenTypes.LEFT_CURL_BRACKET; }
    "}"                         {
                                   if (!braceCounter.isEmpty()) {
                                       if (MajicTokenTypes.LEFT_CURL_BRACKET_SS == braceCounter.peek() && !statusStack.isEmpty()) yybegin(statusStack.pop());
                                       braceCounter.pop();
                                       return MajicTokenTypes.RIGHT_CURL_BRACKET;
                                   } else {
                                       return MajicTokenTypes.BAD_CHARACTER;
                                   }
                                }
    "("                         { braceCounter.push(MajicTokenTypes.LEFT_PARENTHESIS); return MajicTokenTypes.LEFT_PARENTHESIS; }
    ")"                         {
                                    if (!braceCounter.isEmpty() && MajicTokenTypes.LEFT_PARENTHESIS == braceCounter.peek()) {
                                        braceCounter.pop(); return MajicTokenTypes.RIGHT_PARENTHESIS;
                                    } else {
                                        return MajicTokenTypes.BAD_CHARACTER;
                                    }
                                }



    {INTEGER_LITERAL}           { return MajicTokenTypes.INTEGER_NUMBER; }
    {IDENTIFIER}                { return MajicTokenTypes.IDENTIFIER; }
    {STRING_LITERAL}            { return MajicTokenTypes.STRING; }
    {WHITE_SPACE_EOL}+          { return MajicTokenTypes.WHITE_SPACE; }

    {NON_WHITE_SPACE}           { return MajicTokenTypes.BAD_CHARACTER; }
}

<FACTORY_DEFINITION> {
    {ONE_LINE_COMMENT}          { return MajicTokenTypes.COMMENT; }
    {MULTI_LINE_COMMENT}        { return MajicTokenTypes.COMMENT; }
    "DOMSET"                    { return MajicTokenTypes.KEYWORD_DOMSET; }
    "STANDARD_LISTS"            { return MajicTokenTypes.KEYWORD_STANDARD_LISTS; }
    "SORT_BY"                   { return MajicTokenTypes.KEYWORD_SORT_BY; }
    "MLIST"                     { return MajicTokenTypes.KEYWORD_MLIST; }
    "RLIST"                     { return MajicTokenTypes.KEYWORD_RLIST; }
    "OFF"                       { return MajicTokenTypes.KEYWORD_OFF; }
    "ON"                        { return MajicTokenTypes.KEYWORD_ON; }
    "STATIC"                    { return MajicTokenTypes.KEYWORD_STATIC; }
    "DYNAMIC"                   { return MajicTokenTypes.KEYWORD_DYNAMIC; }
    "DISPLAY_NAME"              { return MajicTokenTypes.KEYWORD_DISPLAY_NAME; }
    "FETCH"                     { return MajicTokenTypes.KEYWORD_FETCH; }
    "MAX_FETCH"                 { return MajicTokenTypes.KEYWORD_MAX_FETCH; }
    "WHERE"                     { return MajicTokenTypes.KEYWORD_WHERE; }
    "DISTINCT"                  { return MajicTokenTypes.KEYWORD_DISTINCT; }
    "PARAM_NAMES"               { return MajicTokenTypes.KEYWORD_PARAM_NAMES; }
    "FUNCTION_GROUP"            { return MajicTokenTypes.KEYWORD_FUNCTION_GROUP; }
    "REL_ATTR"                  { return MajicTokenTypes.KEYWORD_REL_ATTR; }
    "COMMON_NAME"               { return MajicTokenTypes.KEYWORD_COMMON_NAME; }
    "RESTRICT"                  { return MajicTokenTypes.KEYWORD_RESTRICT; }
    "LREL"                      { return MajicTokenTypes.KEYWORD_LREL; }
    "METHODS"                   { statusStack.push(FACTORY_DEFINITION); yybegin(METHODS_DEFINITION); return MajicTokenTypes.KEYWORD_METHODS; }

    ";"                         {
                                  if (waitForStatusSwitch) {
                                    if (!statusStack.isEmpty()) yybegin(statusStack.pop());
                                    waitForStatusSwitch = false;
                                  }
                                  return MajicTokenTypes.SEMICOLON;
                                }
    "="                         { return MajicTokenTypes.OP_ASSIGN; }
    ","                         { return MajicTokenTypes.COLON; }

    ////////////// braces ////////////////
    "{"                         { if (waitForStatusSwitch) {
                                    braceCounter.push(MajicTokenTypes.LEFT_CURL_BRACKET_SS);
                                    waitForStatusSwitch = false;
                                  } else {
                                    braceCounter.push(MajicTokenTypes.LEFT_CURL_BRACKET);
                                  }
                                  return MajicTokenTypes.LEFT_CURL_BRACKET; }
    "}"                         {
                                   if (!braceCounter.isEmpty()) {
                                       if (MajicTokenTypes.LEFT_CURL_BRACKET_SS == braceCounter.peek() && !statusStack.isEmpty()) yybegin(statusStack.pop());
                                       braceCounter.pop();
                                       return MajicTokenTypes.RIGHT_CURL_BRACKET;
                                   } else {
                                       return MajicTokenTypes.BAD_CHARACTER;
                                   }
                                }
    "("                         { braceCounter.push(MajicTokenTypes.LEFT_PARENTHESIS); return MajicTokenTypes.LEFT_PARENTHESIS; }
    ")"                         {
                                    if (!braceCounter.isEmpty() && MajicTokenTypes.LEFT_PARENTHESIS == braceCounter.peek()) {
                                        braceCounter.pop(); return MajicTokenTypes.RIGHT_PARENTHESIS;
                                    } else {
                                        return MajicTokenTypes.BAD_CHARACTER;
                                    }
                                }

    {INTEGER_LITERAL}           { return MajicTokenTypes.INTEGER_NUMBER; }

    {IDENTIFIER}                { return MajicTokenTypes.IDENTIFIER; }
    {STRING_LITERAL}            { return MajicTokenTypes.STRING; }
    {WHITE_SPACE_EOL}+          { return MajicTokenTypes.WHITE_SPACE; }

    {NON_WHITE_SPACE}           { return MajicTokenTypes.BAD_CHARACTER; }

}

<METHODS_DEFINITION> {
    {ONE_LINE_COMMENT}          { return MajicTokenTypes.COMMENT; }
    {MULTI_LINE_COMMENT}        { return MajicTokenTypes.COMMENT; }

    ////////////// braces ////////////////
    "{"                         { return MajicTokenTypes.LEFT_CURL_BRACKET; }
    "}"                         {
                                  if (!statusStack.isEmpty()) yybegin(statusStack.pop());
                                  return MajicTokenTypes.RIGHT_CURL_BRACKET; }
    "("                         { braceCounter.push(MajicTokenTypes.LEFT_PARENTHESIS); return MajicTokenTypes.LEFT_PARENTHESIS; }
    ")"                         {
                                    if (!braceCounter.isEmpty() && MajicTokenTypes.LEFT_PARENTHESIS == braceCounter.peek()) {
                                        braceCounter.pop(); return MajicTokenTypes.RIGHT_PARENTHESIS;
                                    } else {
                                        return MajicTokenTypes.BAD_CHARACTER;
                                    }
                                }

    ";"                         { return MajicTokenTypes.SEMICOLON; }
    ","                         { return MajicTokenTypes.COLON; }
    "|"                         { return MajicTokenTypes.PIPE; }
    "..."                       { return MajicTokenTypes.OP_TRIDOT; }

    {IDENTIFIER}                { return MajicTokenTypes.IDENTIFIER; }
    {WHITE_SPACE_EOL}+          { return MajicTokenTypes.WHITE_SPACE; }
    {NON_WHITE_SPACE}           { return MajicTokenTypes.BAD_CHARACTER; }
}