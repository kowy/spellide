package com.datechsw.spellide.lang.majic.lexer;

import com.intellij.lexer.FlexAdapter;
import com.intellij.lexer.MergingLexerAdapter;

public class MajicHighlighterLexer extends MergingLexerAdapter {
    public MajicHighlighterLexer() {
        super(new FlexAdapter(new _MajicLexer((java.io.Reader)null)), MajicTokenTypes.COMMENTS);
    }
}
