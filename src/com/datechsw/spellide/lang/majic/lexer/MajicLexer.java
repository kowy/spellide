package com.datechsw.spellide.lang.majic.lexer;

import com.intellij.lexer.FlexAdapter;

import java.io.Reader;

/**
 * Initialization and configuration of the Majic lexer. It uses output generated from
 * lexical generator JFlex. Use this command to generate lexer:
 *    $FLEX_PATH/bin/jflex --charat --nobak --skel $FLEX_PATH/../idea-flex.skeleton Majic.flex
 */
public class MajicLexer extends FlexAdapter {
    public MajicLexer() {
        super(new _MajicLexer((Reader)null));
    }
}