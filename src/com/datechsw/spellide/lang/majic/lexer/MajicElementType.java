package com.datechsw.spellide.lang.majic.lexer;

import com.datechsw.spellide.lang.majic.MajicFileType;
import com.datechsw.spellide.lang.majic.MajicLanguage;
import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

/**
 * Class for Majic specific lexer tokens. Each lexer token for Majic language
 * is instance of this class.
 * 
 * @see com.datechsw.spellide.lang.majic.lexer.MajicTokenTypes
 */
public class MajicElementType extends IElementType {
    public MajicElementType(@NotNull @NonNls String debugName) {
        super(debugName, MajicFileType.INSTANCE.getLanguage());
    }

    @Override
    public String toString() {
        return MajicLanguage.LANG_NAME + ": " + super.toString();
    }
}