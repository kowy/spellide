package com.datechsw.spellide.lang.majic.editor;

import com.datechsw.spellide.SpellIdeBundle;
import com.datechsw.spellide.lang.majic.MajicFileType;
import com.datechsw.spellide.lang.majic.MajicLanguage;
import com.datechsw.spellide.lang.majic.highlighter.MajicHighlighterColors;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import com.intellij.openapi.fileTypes.SyntaxHighlighterFactory;
import com.intellij.openapi.options.colors.AttributesDescriptor;
import com.intellij.openapi.options.colors.ColorDescriptor;
import com.intellij.openapi.options.colors.ColorSettingsPage;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.util.HashSet;
import java.util.Map;

/**
 * This class add new configuration section "Majic" to Editor - Colors & Fonts
 */
public class MajicColorsPage implements ColorSettingsPage{
    private static final HashSet<AttributesDescriptor> ATTRS;

    static {
        ATTRS = new HashSet<AttributesDescriptor>();
        ATTRS.add(new AttributesDescriptor(SpellIdeBundle.message("highlighter.colorPage.badCharacter"), MajicHighlighterColors.BAD_CHARACTER));
        ATTRS.add(new AttributesDescriptor(SpellIdeBundle.message("highlighter.colorPage.braces"), MajicHighlighterColors.CURL_BRACKETS));
        ATTRS.add(new AttributesDescriptor(SpellIdeBundle.message("highlighter.colorPage.comment"), MajicHighlighterColors.COMMENT));
        ATTRS.add(new AttributesDescriptor(SpellIdeBundle.message("highlighter.colorPage.keywords"), MajicHighlighterColors.KEYWORDS));
        ATTRS.add(new AttributesDescriptor(SpellIdeBundle.message("highlighter.colorPage.numbers"), MajicHighlighterColors.NUMBERS));
        ATTRS.add(new AttributesDescriptor(SpellIdeBundle.message("highlighter.colorPage.operationSign"), MajicHighlighterColors.OPERATORS));
        ATTRS.add(new AttributesDescriptor(SpellIdeBundle.message("highlighter.colorPage.operationTridot"), MajicHighlighterColors.OPERATOR_TRIDOT));
        ATTRS.add(new AttributesDescriptor(SpellIdeBundle.message("highlighter.colorPage.parenths"), MajicHighlighterColors.PARENTHS));
        ATTRS.add(new AttributesDescriptor(SpellIdeBundle.message("highlighter.colorPage.string"), MajicHighlighterColors.STRING));
    }

    @NotNull
    @Override
    public String getDisplayName() {
        return MajicLanguage.LANG_NAME;
    }

    @Override
    public Icon getIcon() {
        return MajicFileType.INSTANCE.getIcon();
    }

    @NotNull
    @Override
    public AttributesDescriptor[] getAttributeDescriptors() {
        return ATTRS.toArray(new AttributesDescriptor[] {});
    }

    @NotNull
    @Override
    public ColorDescriptor[] getColorDescriptors() {
        return ColorDescriptor.EMPTY_ARRAY;
    }

    @NotNull
    @Override
    public SyntaxHighlighter getHighlighter() {
        return SyntaxHighlighterFactory.getSyntaxHighlighter(MajicLanguage.INSTANCE, null, null);
    }

    @NotNull
    @Override
    public String getDemoText() {
        return "/* block comment */\n" +
               "OBJECT obj {\n" +
               "    ATTRIBUTES {\n" +
               "        del inactive SREL actbool REQUIRED {\n" +
               "            ON_NEW DEFAULT 0 ;\n" +
               "        } ;\n" +
               "        version_number UNKNOWN_TYPE;\n" +
               "    } // missing semicolon\n" +
               "\n" +
               "    FACTORY fac {\n" +
               "        RESTRICT \"type = 2305\" { type = 2305} ;\n" +
               "        @@@  // bad characters\n" +
               "        METHODS {\n" +
               "            method_name( string, int, ... ) ;\n" +
               "            next_method( ... ) ;\n" +
               "        } ;\n" +
               "    } ;\n" +
               "} ;";
    }

    @Override
    public Map<String, TextAttributesKey> getAdditionalHighlightingTagToDescriptorMap() {
        return null;
    }

}
