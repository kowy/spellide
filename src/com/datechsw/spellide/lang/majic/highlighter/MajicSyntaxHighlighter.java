package com.datechsw.spellide.lang.majic.highlighter;

import com.datechsw.spellide.lang.majic.lexer.MajicHighlighterLexer;
import com.datechsw.spellide.lang.majic.lexer.MajicTokenTypes;
import com.intellij.lexer.Lexer;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase;
import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

/**
 * Majic language syntax highlighter for IntelliJ Idea editor
 */
public class MajicSyntaxHighlighter extends SyntaxHighlighterBase {
    public static final Map<IElementType, TextAttributesKey> colors = new HashMap<IElementType, TextAttributesKey>();
    public static final Map<IElementType, TextAttributesKey> backgrounds = new HashMap<IElementType, TextAttributesKey>();

    public MajicSyntaxHighlighter() {
        // add brackets
        fillMap(colors, MajicTokenTypes.PARENTHESIS, MajicHighlighterColors.PARENTHS);
        fillMap(colors, MajicTokenTypes.CURL_BRACKETS, MajicHighlighterColors.CURL_BRACKETS);

        colors.put(MajicTokenTypes.BAD_CHARACTER, MajicHighlighterColors.BAD_CHARACTER);
        colors.put(MajicTokenTypes.STRING, MajicHighlighterColors.STRING);
        // TODO Extra zvýraznění pro VALID_STRING_ESCAPE (\n) a VALID_STRING_PLACEHOLDER (%1)
        fillMap(colors, MajicTokenTypes.NUMBERS, MajicHighlighterColors.NUMBERS);

        fillMap(colors, MajicTokenTypes.COMMENTS, MajicHighlighterColors.COMMENT);
        fillMap(colors, MajicTokenTypes.OPERATORS, MajicHighlighterColors.OPERATORS);
        colors.put(MajicTokenTypes.OP_TRIDOT, MajicHighlighterColors.OPERATOR_TRIDOT);
        fillMap(colors, MajicTokenTypes.KEYWORDS, MajicHighlighterColors.KEYWORDS);
    }
    
    @NotNull
    @Override
    public Lexer getHighlightingLexer() {
        return new MajicHighlighterLexer();
    }

    @NotNull
    @Override
    public TextAttributesKey[] getTokenHighlights(IElementType tokenType) {
        return pack(colors.get(tokenType), backgrounds.get(tokenType));
    }
}
