package com.datechsw.spellide.lang.majic.highlighter;

import com.intellij.openapi.editor.DefaultLanguageHighlighterColors;
import com.intellij.openapi.editor.HighlighterColors;
import com.intellij.openapi.editor.colors.TextAttributesKey;

/**
 * This interface contains color and font definitions for lexical types
 * used by Majic highlighter;
 */
public interface MajicHighlighterColors {
    TextAttributesKey BAD_CHARACTER = HighlighterColors.BAD_CHARACTER;

    TextAttributesKey COMMENT = TextAttributesKey.createTextAttributesKey(
            "MAJIC.COMMENT", DefaultLanguageHighlighterColors.BLOCK_COMMENT);
    TextAttributesKey STRING = TextAttributesKey.createTextAttributesKey(
            "MAJIC.STRING", DefaultLanguageHighlighterColors.STRING);
    TextAttributesKey STRING_ESCAPE = TextAttributesKey.createTextAttributesKey(
            "MAJIC.STRING_ESCAPE", DefaultLanguageHighlighterColors.VALID_STRING_ESCAPE);
    TextAttributesKey OPERATORS = TextAttributesKey.createTextAttributesKey(
            "MAJIC.OPERATION_SIGN", DefaultLanguageHighlighterColors.OPERATION_SIGN);
    // TODO: udělej speciální barvení pro operátor trojtečka
    TextAttributesKey OPERATOR_TRIDOT = TextAttributesKey.createTextAttributesKey(
            "MAJIC.OPERATOR_TRIDOT", DefaultLanguageHighlighterColors.OPERATION_SIGN);
    TextAttributesKey CURL_BRACKETS = TextAttributesKey.createTextAttributesKey(
            "MAJIC.CURL_BRACKETS", DefaultLanguageHighlighterColors.BRACES);
    TextAttributesKey PARENTHS = TextAttributesKey.createTextAttributesKey(
            "MAJIC.PARENTHS", DefaultLanguageHighlighterColors.PARENTHESES);
    TextAttributesKey KEYWORDS = TextAttributesKey.createTextAttributesKey(
            "MAJIC.KEYWORDS", DefaultLanguageHighlighterColors.KEYWORD);
    TextAttributesKey NUMBERS = TextAttributesKey.createTextAttributesKey(
            "MAJIC.NUMBERS", DefaultLanguageHighlighterColors.NUMBER);
}
