package com.datechsw.spellide.lang.majic.highlighter;

import com.datechsw.spellide.SpellIdeBundle;
import com.datechsw.spellide.lang.majic.psi.MajicElement;
import com.datechsw.spellide.lang.majic.psi.MajicObjectStatement;
import com.datechsw.spellide.lang.majic.psi.MajicTriggerStatement;
import com.datechsw.spellide.lang.majic.visitor.MajicElementVisitor;
import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.lang.annotation.Annotator;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.psi.PsiElement;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

/**
 * Majic Anotator tries to investigate semantic or more complex syntax analysis and
 * generates Errors, Warnings and QuickFixes if some issues found
 */
public class MajicAnnotator extends MajicElementVisitor implements Annotator {
    private AnnotationHolder currentHolder = null;
    private static final Logger log = Logger.getInstance(MajicAnnotator.class);

    @Override
    public void annotate(@NotNull PsiElement element, @NotNull AnnotationHolder holder) {
        if (element instanceof MajicElement) {
            currentHolder = holder;
            ((MajicElement) element).accept(this);
            currentHolder = null;
        }
    }

    /**
     * This method is called when MajicTriggerStatement element is parsed
     * Check if sequence number of a trigger definition is unique in object
     */
    @Override
    public void visitTriggerStatement(MajicTriggerStatement trigger) {
        super.visitTriggerStatement(trigger);

        MajicObjectStatement obj = trigger.getOwningObject();
        if (obj != null) {
            if (obj.getTriggers().containsKey(trigger.getSequenceNumber())) {
                HashMap<Integer, MajicTriggerStatement> triggers = obj.getTriggers();
                if (! trigger.getName().equals(triggers.get(trigger.getSequenceNumber()).getName())) {
                    currentHolder.createWarningAnnotation(trigger, SpellIdeBundle.message("annotator.trigger.sequence_already_used", trigger.getSequenceNumber(), triggers.get(trigger.getSequenceNumber()).getName()));
                }
            }
        }
    }
}
