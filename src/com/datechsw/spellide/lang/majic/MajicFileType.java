package com.datechsw.spellide.lang.majic;

import com.datechsw.spellide.SpellIdeBundle;
import com.google.common.collect.ImmutableList;
import com.intellij.openapi.fileTypes.LanguageFileType;
import com.intellij.openapi.util.IconLoader;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.util.List;

/**
 * MOD file type definition and configuration
 */
public class MajicFileType extends LanguageFileType {
    @NonNls
    public static final List<String> DEFAULT_EXTENSIONS = ImmutableList.of("mod", "maj" );
    public static final LanguageFileType INSTANCE = new MajicFileType();
    private static final Icon FILE_ICON = IconLoader.getIcon("images/fileTypes/majic.png");
    public static final String MIME_TYPE = "text/maj";

    private MajicFileType() {
        super(MajicLanguage.INSTANCE);
    }

    @NotNull
    @Override
    public String getName() {
        return MajicLanguage.LANG_NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return SpellIdeBundle.message("majic.filetype.description");
    }

    @NotNull
    @Override
    public String getDefaultExtension() {
        return DEFAULT_EXTENSIONS.get(0);
    }

    @Override
    public Icon getIcon() {
        return FILE_ICON;
    }
}
