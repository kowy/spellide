package com.datechsw.spellide.lang.spell.psi.impl;

import com.datechsw.spellide.lang.spell.SpellLanguage;
import com.datechsw.spellide.lang.spell.psi.SpellElement;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import com.intellij.lang.Language;
import org.jetbrains.annotations.NotNull;

/**
 * Base element of the Spell PSI infrastructure
 */
public class SpellElementImpl extends ASTWrapperPsiElement implements SpellElement {

    public static final String IMPL_EXTENSION = "Impl";

    public SpellElementImpl(@NotNull ASTNode node) {
        super(node);
    }

    @NotNull
    @Override
    public Language getLanguage() {
        return SpellLanguage.INSTANCE;
    }

    @Override
    public String toString() {
        String name = getClass().getName();
        if (name.endsWith(IMPL_EXTENSION)) {
            name = name.substring(0, name.length() - IMPL_EXTENSION.length());
        }

        name = name.substring(name.lastIndexOf('.') + 1);

        return name;
    }
}
