package com.datechsw.spellide.lang.spell.psi.impl;

import com.datechsw.spellide.lang.spell.lexer.SpellTokenTypes;
import com.datechsw.spellide.lang.spell.psi.SpellMacroDefinition;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.util.IncorrectOperationException;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

/**
 * Implementation of PSI Named Element providing extra information about Macro definition
 */
public class SpellMacroDefinitionImpl extends SpellElementImpl implements SpellMacroDefinition{
    public SpellMacroDefinitionImpl(@NotNull ASTNode node) {
        super(node);
    }

    @Override
    public PsiElement getNameElement() {
        ASTNode node = getNode().findChildByType(SpellTokenTypes.MACRO_STATEMENT);
        return node != null ? node.getPsi() : null;
    }

    @Override
    public PsiElement setName(@NonNls @NotNull String name) throws IncorrectOperationException {
        throw new IncorrectOperationException();
    }
}
