package com.datechsw.spellide.lang.spell.psi.impl;

import com.datechsw.spellide.lang.spell.SpellFileType;
import com.datechsw.spellide.lang.spell.psi.SpellPsiFile;
import com.intellij.extapi.psi.PsiFileBase;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.psi.FileViewProvider;
import org.jetbrains.annotations.NotNull;

/**
 * Implementation of a top level element of AST tree - PsiFileBase
 */
public class SpellPsiFileImpl extends PsiFileBase implements SpellPsiFile {

    public SpellPsiFileImpl(FileViewProvider viewProvider) {
        super(viewProvider, SpellFileType.INSTANCE.getLanguage());
    }

    @NotNull
    @Override
    public FileType getFileType() {
        return SpellFileType.INSTANCE;
    }
}
