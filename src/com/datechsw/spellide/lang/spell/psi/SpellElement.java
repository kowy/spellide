package com.datechsw.spellide.lang.spell.psi;

import com.intellij.psi.PsiElement;

/**
 * Interface for a base element of the Spell PSI infrastructure
 */
public interface SpellElement extends PsiElement {
}
