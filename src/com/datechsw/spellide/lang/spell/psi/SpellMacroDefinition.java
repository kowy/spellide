package com.datechsw.spellide.lang.spell.psi;

import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiNamedElement;

/**
 * Interface for PSI Named Element providing extra information about Macro definition
 */
public interface SpellMacroDefinition extends SpellElement, PsiNamedElement {
    PsiElement getNameElement();
}
