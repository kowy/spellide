package com.datechsw.spellide.lang.spell;

import com.datechsw.spellide.SpellIdeBundle;
import com.intellij.openapi.fileTypes.LanguageFileType;
import com.intellij.openapi.util.IconLoader;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

/**
 * Spell file type definition and configuration
 */
public class SpellFileType extends LanguageFileType {
    @NonNls public static final String DEFAULT_EXTENSION = "spl";
    public static final LanguageFileType INSTANCE = new SpellFileType();
    private static final Icon FILE_ICON = IconLoader.getIcon("images/fileTypes/spell.png");
    public static final String MIME_TYPE = "text/spell";

    private SpellFileType() {
        super(SpellLanguage.INSTANCE);
    }

    @NotNull
    @Override
    public String getName() {
        return SpellLanguage.LANG_NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return SpellIdeBundle.message("spell.filetype.description");
    }

    @NotNull
    @Override
    public String getDefaultExtension() {
        return DEFAULT_EXTENSION;
    }

    @Override
    public Icon getIcon() {
        return FILE_ICON;
    }
}