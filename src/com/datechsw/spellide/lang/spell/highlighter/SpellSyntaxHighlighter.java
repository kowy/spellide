package com.datechsw.spellide.lang.spell.highlighter;

import com.datechsw.spellide.lang.spell.lexer.SpellHighlighterLexer;
import com.datechsw.spellide.lang.spell.lexer.SpellTokenTypes;
import com.intellij.lexer.Lexer;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase;
import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

/**
 * Spell language syntax highlighter for IntelliJ Idea editor
 */
public class SpellSyntaxHighlighter extends SyntaxHighlighterBase {
    public static final Map<IElementType, TextAttributesKey> colors = new HashMap<IElementType, TextAttributesKey>();
    public static final Map<IElementType, TextAttributesKey> backgrounds = new HashMap<IElementType, TextAttributesKey>();

    public SpellSyntaxHighlighter() {
        // add brackets
        fillMap(colors, SpellTokenTypes.PARENTHESIS, SpellHighlighterColors.PARENTHS);
        fillMap(colors, SpellTokenTypes.CURL_BRACKETS, SpellHighlighterColors.CURL_BRACKETS);
        fillMap(colors, SpellTokenTypes.SQUARE_BRACKETS, SpellHighlighterColors.SQ_BRACKETS);

        colors.put(SpellTokenTypes.BAD_CHARACTER, SpellHighlighterColors.BAD_CHARACTER);
        colors.put(SpellTokenTypes.STRING, SpellHighlighterColors.STRING);
        // TODO Extra zvýraznění pro VALID_STRING_ESCAPE (\n) a VALID_STRING_PLACEHOLDER (%1)
        fillMap(colors, SpellTokenTypes.NUMBERS, SpellHighlighterColors.NUMBERS);

        fillMap(colors, SpellTokenTypes.COMMENTS, SpellHighlighterColors.COMMENT);
        fillMap(colors, SpellTokenTypes.OPERATORS, SpellHighlighterColors.OPERATION_SIGN);
        fillMap(colors, SpellTokenTypes.KEYWORDS, SpellHighlighterColors.KEYWORDS);

        colors.put(SpellTokenTypes.OP_DOT, SpellHighlighterColors.OPERATION_DOT);
        colors.put(SpellTokenTypes.OP_TRIDOT, SpellHighlighterColors.OPERATION_TRIDOT);
        colors.put(SpellTokenTypes.OP_FOURDOT, SpellHighlighterColors.OPERATION_FOURDOT);
        colors.put(SpellTokenTypes.OP_COMMA, SpellHighlighterColors.OPERATION_COMMA);
    }

    @NotNull
    @Override
    public Lexer getHighlightingLexer() {
        return new SpellHighlighterLexer();
    }

    @NotNull
    @Override
    public TextAttributesKey[] getTokenHighlights(IElementType tokenType) {
        return pack(colors.get(tokenType), backgrounds.get(tokenType));
    }
}
