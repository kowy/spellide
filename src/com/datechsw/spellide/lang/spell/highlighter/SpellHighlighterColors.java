package com.datechsw.spellide.lang.spell.highlighter;

import com.intellij.openapi.editor.HighlighterColors;
import com.intellij.openapi.editor.SyntaxHighlighterColors;
import com.intellij.openapi.editor.colors.TextAttributesKey;

/**
 * This interface contains color and font definitions for lexical types
 * used by Syntax highlighter
 */
public interface SpellHighlighterColors {
    TextAttributesKey BAD_CHARACTER = HighlighterColors.BAD_CHARACTER;

    TextAttributesKey COMMENT = TextAttributesKey.createTextAttributesKey(
            "SPELL.COMMENT", SyntaxHighlighterColors.JAVA_BLOCK_COMMENT.getDefaultAttributes());
    TextAttributesKey STRING = TextAttributesKey.createTextAttributesKey(
            "SPELL.STRING", SyntaxHighlighterColors.STRING.getDefaultAttributes());
    TextAttributesKey STRING_ESCAPE = TextAttributesKey.createTextAttributesKey(
            "SPELL.STRING_ESCAPE", SyntaxHighlighterColors.VALID_STRING_ESCAPE.getDefaultAttributes());
    TextAttributesKey OPERATION_SIGN = TextAttributesKey.createTextAttributesKey(
            "SPELL.OPERATION_SIGN", SyntaxHighlighterColors.OPERATION_SIGN.getDefaultAttributes());
    TextAttributesKey OPERATION_DOT = TextAttributesKey.createTextAttributesKey(
            "SPELL.OPERATION_DOT", SyntaxHighlighterColors.OPERATION_SIGN.getDefaultAttributes());
    TextAttributesKey OPERATION_TRIDOT = TextAttributesKey.createTextAttributesKey(
            "SPELL.OPERATION_TRIDOT", SyntaxHighlighterColors.OPERATION_SIGN.getDefaultAttributes());
    TextAttributesKey OPERATION_FOURDOT = TextAttributesKey.createTextAttributesKey(
            "SPELL.OPERATION_FOURDOT", SyntaxHighlighterColors.OPERATION_SIGN.getDefaultAttributes());
    TextAttributesKey OPERATION_COMMA = TextAttributesKey.createTextAttributesKey(
            "SPELL.OPERATION_COMMA", SyntaxHighlighterColors.OPERATION_SIGN.getDefaultAttributes());
    TextAttributesKey CURL_BRACKETS = TextAttributesKey.createTextAttributesKey(
            "SPELL.CURL_BRACKETS", SyntaxHighlighterColors.BRACES.getDefaultAttributes());
    TextAttributesKey SQ_BRACKETS = TextAttributesKey.createTextAttributesKey(
            "SPELL.SQUARE_BRACKETS", SyntaxHighlighterColors.BRACKETS.getDefaultAttributes());
    TextAttributesKey PARENTHS = TextAttributesKey.createTextAttributesKey(
            "SPELL.PARENTHS", SyntaxHighlighterColors.PARENTHS.getDefaultAttributes());
    TextAttributesKey KEYWORDS = TextAttributesKey.createTextAttributesKey(
            "SPELL.KEYWORDS", SyntaxHighlighterColors.KEYWORD.getDefaultAttributes());
    TextAttributesKey NUMBERS = TextAttributesKey.createTextAttributesKey(
            "SPELL.NUMBERS", SyntaxHighlighterColors.NUMBER.getDefaultAttributes());


}
