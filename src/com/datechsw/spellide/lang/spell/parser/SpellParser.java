package com.datechsw.spellide.lang.spell.parser;

import com.datechsw.spellide.SpellIdeBundle;
import com.datechsw.spellide.lang.spell.lexer.SpellTokenTypes;
import com.intellij.lang.ASTNode;
import com.intellij.lang.PsiBuilder;
import com.intellij.lang.PsiParser;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NotNull;

/**
 * Implementation of Spell parser. Here is built an AST Tree from Lexer output
 */
public class SpellParser implements PsiParser {
    private static final Logger log = Logger.getInstance(SpellParser.class);

    @NotNull
    @Override
    public ASTNode parse(IElementType root, PsiBuilder builder) {
        final PsiBuilder.Marker rootMarker = builder.mark();
        // TODO: remove Debug flag
        builder.setDebugMode(true);

        while (!builder.eof()) {
            if (builder.getTokenType() == SpellTokenTypes.KEYWORD_MACRO) {
                parseMacroDefinition(builder);
            } else {
                //PsiBuilder.Marker marker = builder.mark();
                builder.advanceLexer();
                //marker.done(builder.getTokenType());
            }
        }

        rootMarker.done(root);
        return builder.getTreeBuilt();
    }

    private void parseMacroDefinition(PsiBuilder builder) {
        log.assertTrue(builder.getTokenType() == SpellTokenTypes.KEYWORD_MACRO);
        final PsiBuilder.Marker macroName = builder.mark();
        builder.advanceLexer();
        if (builder.getTokenType() != SpellTokenTypes.IDENTIFIER) {
            builder.error(SpellIdeBundle.message("parser.expected.identifier"));
            macroName.drop();
        } else {
            builder.advanceLexer();
        }

        if (builder.getTokenType() != SpellTokenTypes.MACRO_VALUE) {
            builder.error(SpellIdeBundle.message("parser.expected.macro.definition"));
            macroName.drop();
        } else {
            builder.advanceLexer();
        }

        macroName.done(SpellTokenTypes.MACRO_STATEMENT);
    }
}
