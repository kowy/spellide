package com.datechsw.spellide.lang.spell.parser;

import com.datechsw.spellide.lang.spell.lexer.SpellLexer;
import com.datechsw.spellide.lang.spell.lexer.SpellTokenTypes;
import com.datechsw.spellide.lang.spell.psi.impl.SpellMacroDefinitionImpl;
import com.datechsw.spellide.lang.spell.psi.impl.SpellPsiFileImpl;
import com.intellij.lang.ASTNode;
import com.intellij.lang.ParserDefinition;
import com.intellij.lang.PsiParser;
import com.intellij.lexer.Lexer;
import com.intellij.openapi.project.Project;
import com.intellij.psi.FileViewProvider;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.tree.IElementType;
import com.intellij.psi.tree.IFileElementType;
import com.intellij.psi.tree.TokenSet;
import org.jetbrains.annotations.NotNull;

/**
 * Initialization and configuration of Spell parser
 */
public class SpellParserDefinition implements ParserDefinition {
    @NotNull
    @Override
    public Lexer createLexer(Project project) {
        return new SpellLexer();
    }

    @Override
    public PsiParser createParser(Project project) {
        return new SpellParser();
    }

    @Override
    public IFileElementType getFileNodeType() {
        return SpellTokenTypes.FILE;
    }

    @NotNull
    @Override
    public TokenSet getWhitespaceTokens() {
        return SpellTokenTypes.WHITESPACES;
    }

    @NotNull
    @Override
    public TokenSet getCommentTokens() {
        return SpellTokenTypes.COMMENTS;
    }

    @NotNull
    @Override
    public TokenSet getStringLiteralElements() {
        return TokenSet.EMPTY;
    }

    @NotNull
    @Override
    public PsiElement createElement(ASTNode node) {
        final IElementType type = node.getElementType();

        return new SpellMacroDefinitionImpl(node);
    }

    @Override
    public PsiFile createFile(FileViewProvider viewProvider) {
        return new SpellPsiFileImpl(viewProvider);
    }

    @Override
    public SpaceRequirements spaceExistanceTypeBetweenTokens(ASTNode left, ASTNode right) {
        return SpaceRequirements.MAY;
    }
}