package com.datechsw.spellide.lang.spell.editor;

import com.datechsw.spellide.SpellIdeBundle;
import com.datechsw.spellide.lang.spell.SpellFileType;
import com.datechsw.spellide.lang.spell.SpellLanguage;
import com.datechsw.spellide.lang.spell.highlighter.SpellHighlighterColors;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import com.intellij.openapi.fileTypes.SyntaxHighlighterFactory;
import com.intellij.openapi.options.colors.AttributesDescriptor;
import com.intellij.openapi.options.colors.ColorDescriptor;
import com.intellij.openapi.options.colors.ColorSettingsPage;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.util.HashSet;
import java.util.Map;

/**
 * This class add new configuration section "Spell" to Editor - Colors & Fonts
 */
public class SpellColorsPage implements ColorSettingsPage {
    private static final HashSet<AttributesDescriptor> ATTRS;

    static {
        ATTRS = new HashSet<AttributesDescriptor>();
        ATTRS.add(new AttributesDescriptor(SpellIdeBundle.message("highlighter.colorPage.badCharacter"), SpellHighlighterColors.BAD_CHARACTER));
        ATTRS.add(new AttributesDescriptor(SpellIdeBundle.message("highlighter.colorPage.braces"), SpellHighlighterColors.CURL_BRACKETS));
        ATTRS.add(new AttributesDescriptor(SpellIdeBundle.message("highlighter.colorPage.brackets"), SpellHighlighterColors.SQ_BRACKETS));
        ATTRS.add(new AttributesDescriptor(SpellIdeBundle.message("highlighter.colorPage.comment"), SpellHighlighterColors.COMMENT));
        ATTRS.add(new AttributesDescriptor(SpellIdeBundle.message("highlighter.colorPage.keywords"), SpellHighlighterColors.KEYWORDS));
        ATTRS.add(new AttributesDescriptor(SpellIdeBundle.message("highlighter.colorPage.numbers"), SpellHighlighterColors.NUMBERS));
        ATTRS.add(new AttributesDescriptor(SpellIdeBundle.message("highlighter.colorPage.operationSign"), SpellHighlighterColors.OPERATION_SIGN));
        ATTRS.add(new AttributesDescriptor(SpellIdeBundle.message("highlighter.colorPage.operationDot"), SpellHighlighterColors.OPERATION_DOT));
        ATTRS.add(new AttributesDescriptor(SpellIdeBundle.message("highlighter.colorPage.operationTridot"), SpellHighlighterColors.OPERATION_TRIDOT));
        ATTRS.add(new AttributesDescriptor(SpellIdeBundle.message("highlighter.colorPage.operationFourdot"), SpellHighlighterColors.OPERATION_FOURDOT));
        ATTRS.add(new AttributesDescriptor(SpellIdeBundle.message("highlighter.colorPage.operationComma"), SpellHighlighterColors.OPERATION_COMMA));
        ATTRS.add(new AttributesDescriptor(SpellIdeBundle.message("highlighter.colorPage.parenths"), SpellHighlighterColors.PARENTHS));
        ATTRS.add(new AttributesDescriptor(SpellIdeBundle.message("highlighter.colorPage.string"), SpellHighlighterColors.STRING));
    }

    @NotNull
    @Override
    public String getDisplayName() {
        return SpellLanguage.LANG_NAME;
    }

    @Override
    public Icon getIcon() {
        return SpellFileType.INSTANCE.getIcon();
    }

    @NotNull
    @Override
    public AttributesDescriptor[] getAttributeDescriptors() {
        return ATTRS.toArray(new AttributesDescriptor[] {});
    }

    @NotNull
    @Override
    public ColorDescriptor[] getColorDescriptors() {
        return ColorDescriptor.EMPTY_ARRAY;
    }

    @NotNull
    @Override
    public SyntaxHighlighter getHighlighter() {
        return SyntaxHighlighterFactory.getSyntaxHighlighter(SpellLanguage.INSTANCE, null, null);
    }

    @NotNull
    @Override
    public String getDemoText() {
        return "// comment\n" +
                "#define DEBUG 1\n" +
                "chg.category::some_method(...) {\n" +
                "    send_wait(0, this, \"get_attr_vals\", 4,\"category.group\", \"category.assignee\", \"category.organization\",\"category.cab\");\n" +
                "    @@@ // bad characters\n" +
                "    /* block comment */\n" +
                "    uuid new_group;\n" +
                "    int i;\n" +
                "    UnknownType unknown;\n" +
                "    logf(TRACE,\"sendwait reply is 1.%s : %u\",msg[1]);\n" +
                "   \n" +
                "    if ( !is_null(msg[3]) ) {\n" +
                "       new_group = msg[3];\n" +
                "       i = i + 1;\n" +
                "    }\n" +
                "}";
    }

    @Override
    public Map<String, TextAttributesKey> getAdditionalHighlightingTagToDescriptorMap() {
        return null;
    }
}
