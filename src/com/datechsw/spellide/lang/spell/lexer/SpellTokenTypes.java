package com.datechsw.spellide.lang.spell.lexer;

import com.datechsw.spellide.lang.spell.SpellLanguage;
import com.intellij.psi.TokenType;
import com.intellij.psi.tree.IElementType;
import com.intellij.psi.tree.IFileElementType;
import com.intellij.psi.tree.IStubFileElementType;
import com.intellij.psi.tree.TokenSet;

/**
 * Interface providing all Token types in Spell language
 */
public interface SpellTokenTypes {
    SpellLanguage LANG = SpellLanguage.INSTANCE;

    IFileElementType FILE = new IStubFileElementType(LANG);

    IElementType WHITE_SPACE = TokenType.WHITE_SPACE;
    IElementType BAD_CHARACTER = TokenType.BAD_CHARACTER;

    IElementType COMMENT = new SpellElementType("COMMENT");
    IElementType DOCUMENTATION_COMMENT = new SpellElementType("DOCUMENTATION_COMMENT");

    IElementType KEYWORD_BREAK = new SpellElementType("KEYWORD_BREAK");
    IElementType KEYWORD_BOOL = new SpellElementType("KEYWORD_BOOL");
    IElementType KEYWORD_CASE = new SpellElementType("KEYWORD_CASE");
    IElementType KEYWORD_CONTINUE = new SpellElementType("KEYWORD_CONTINUE");
    IElementType KEYWORD_DATE = new SpellElementType("KEYWORD_DATE");
    IElementType KEYWORD_DURATION = new SpellElementType("KEYWORD_DURATION");
    IElementType KEYWORD_ELSE = new SpellElementType("KEYWORD_ELSE");
    IElementType KEYWORD_ERROR = new SpellElementType("KEYWORD_ERROR");
    IElementType KEYWORD_FALSE = new SpellElementType("KEYWORD_FALSE");
    IElementType KEYWORD_FATAL = new SpellElementType("KEYWORD_FATAL");
    IElementType KEYWORD_FOR = new SpellElementType("KEYWORD_FOR");
    IElementType KEYWORD_FLOAT = new SpellElementType("KEYWORD_FLOAT");
    IElementType KEYWORD_IF = new SpellElementType("KEYWORD_IF");
    IElementType KEYWORD_INT = new SpellElementType("KEYWORD_INT");
    IElementType KEYWORD_MACRO = new SpellElementType("KEYWORD_MACRO");
    IElementType KEYWORD_MILESTONE = new SpellElementType("KEYWORD_MILESTONE");
    IElementType KEYWORD_OBJECT = new SpellElementType("KEYWORD_OBJECT");
    IElementType KEYWORD_NULL = new SpellElementType("KEYWORD_NULL");
    IElementType KEYWORD_RETURN = new SpellElementType("KEYWORD_RETURN");
    IElementType KEYWORD_SIGNIFICANT = new SpellElementType("KEYWORD_SIGNIFICANT");
    IElementType KEYWORD_STRING = new SpellElementType("KEYWORD_STRING");
    IElementType KEYWORD_SWITCH = new SpellElementType("KEYWORD_SWITCH");
    IElementType KEYWORD_TRACE = new SpellElementType("KEYWORD_TRACE");
    IElementType KEYWORD_TRUE = new SpellElementType("KEYWORD_TRUE");
    IElementType KEYWORD_UUID = new SpellElementType("KEYWORD_UUID");
    IElementType KEYWORD_VERBOSE = new SpellElementType("KEYWORD_VERBOSE");
    IElementType KEYWORD_VOID = new SpellElementType("KEYWORD_VOID");
    IElementType KEYWORD_WARNING = new SpellElementType("KEYWORD_WARNING");
    IElementType KEYWORD_WHILE = new SpellElementType("KEYWORD_WHILE");

    IElementType MACRO_VALUE = new SpellElementType("MACRO_VALUE");

    IElementType LEFT_PARENTHESIS = new SpellElementType("LEFT_PARENTHESIS");
    IElementType RIGHT_PARENTHESIS = new SpellElementType("RIGHT_PARENTHESIS");
    IElementType LEFT_CURL_BRACKET = new SpellElementType("LEFT_CURL_BRACKET");
    IElementType RIGHT_CURL_BRACKET = new SpellElementType("RIGHT_CURL_BRACKET");
    IElementType LEFT_SQ_BRACKET = new SpellElementType("LEFT_SQ_BRACKET");
    IElementType RIGHT_SQ_BRACKET = new SpellElementType("RIGHT_SQ_BRACKET");
    IElementType SEMICOLON = new SpellElementType("SEMICOLON");
    IElementType OP_COMMA = new SpellElementType("OP_COMMA");
    IElementType OP_DOT = new SpellElementType("OP_DOT");
    IElementType OP_TRIDOT = new SpellElementType("OP_TRIDOT");
    IElementType OP_FOURDOT = new SpellElementType("OP_FOURDOT");
    IElementType OP_ADD = new SpellElementType("OP_ADD");
    IElementType OP_SUB = new SpellElementType("OP_SUB");
    IElementType OP_MULT = new SpellElementType("OP_MULT");
    IElementType OP_DIV = new SpellElementType("OP_DIV");
    IElementType OP_MODULO = new SpellElementType("OP_MODULO");
    IElementType OP_EQ = new SpellElementType("OP_EQ");
    IElementType OP_NEQ = new SpellElementType("OP_NEQ");
    IElementType OP_LESS_OR_EQ = new SpellElementType("OP_LESS_OR_EQ");
    IElementType OP_LESS = new SpellElementType("OP_LESS");
    IElementType OP_MORE_OR_EQ = new SpellElementType("OP_MORE_OR_EQ");
    IElementType OP_MORE = new SpellElementType("OP_MORE");
    IElementType OP_LOG_OR = new SpellElementType("OP_LOG_OR");
    IElementType OP_LOG_AND = new SpellElementType("OP_LOG_AND");
    IElementType OP_LOG_NOT = new SpellElementType("OP_LOG_NOT");
    IElementType OP_ASSIGN = new SpellElementType("OP_ASSIGN");
    IElementType OP_ADD_ASSIGN = new SpellElementType("OP_ADD_ASSIGN");
    IElementType OP_SUB_ASSIGN = new SpellElementType("OP_SUB_ASSIGN");
    IElementType OP_MULT_ASSIGN = new SpellElementType("OP_MULT_ASSIGN");
    IElementType OP_DIV_ASSIGN = new SpellElementType("OP_DIV_ASSIGN");
    IElementType OP_MODULO_ASSIGN = new SpellElementType("OP_MODULO_ASSIGN");

    IElementType IDENTIFIER = new SpellElementType("IDENTIFIER");

    IElementType STRING = new SpellElementType("STRING");
    IElementType INTEGER_NUMBER = new SpellElementType("INTEGER_NUMBER");
    IElementType FP_NUMBER = new SpellElementType("FP_NUMBER");  // Floating point number

    ////////////// Composite elements
    IElementType MACRO_STATEMENT = new SpellElementType("MACRO_STATEMENT");

    ////////////// Token Sets
    TokenSet WHITESPACES = TokenSet.create(WHITE_SPACE);
    TokenSet COMMENTS = TokenSet.create(DOCUMENTATION_COMMENT, COMMENT);
    TokenSet PARENTHESIS = TokenSet.create(LEFT_PARENTHESIS, RIGHT_PARENTHESIS);
    TokenSet CURL_BRACKETS = TokenSet.create(LEFT_CURL_BRACKET, RIGHT_CURL_BRACKET);
    TokenSet SQUARE_BRACKETS = TokenSet.create(LEFT_SQ_BRACKET, RIGHT_SQ_BRACKET);

    TokenSet OPERATORS = TokenSet.create(OP_ADD, OP_SUB, OP_MULT, OP_DIV, OP_MODULO,
            OP_EQ, OP_NEQ, OP_LESS_OR_EQ, OP_LESS, OP_MORE_OR_EQ, OP_MORE,
            OP_LOG_OR, OP_LOG_AND, OP_LOG_NOT,
            OP_ASSIGN, OP_ADD_ASSIGN, OP_SUB_ASSIGN, OP_MULT_ASSIGN, OP_DIV_ASSIGN, OP_MODULO_ASSIGN);
    TokenSet KEYWORDS = TokenSet.create( KEYWORD_BREAK, KEYWORD_BOOL, KEYWORD_CASE, KEYWORD_CONTINUE,
            KEYWORD_DATE, KEYWORD_DURATION, KEYWORD_ELSE, KEYWORD_ERROR, KEYWORD_FALSE, KEYWORD_FATAL,
            KEYWORD_FOR, KEYWORD_FLOAT, KEYWORD_IF, KEYWORD_INT, KEYWORD_MACRO, KEYWORD_MILESTONE,
            KEYWORD_OBJECT, KEYWORD_NULL, KEYWORD_RETURN, KEYWORD_SIGNIFICANT, KEYWORD_STRING,
            KEYWORD_SWITCH, KEYWORD_TRACE, KEYWORD_TRUE, KEYWORD_UUID, KEYWORD_VERBOSE, KEYWORD_VOID,
            KEYWORD_WARNING, KEYWORD_WHILE);
    TokenSet NUMBERS = TokenSet.create(INTEGER_NUMBER, FP_NUMBER);
}