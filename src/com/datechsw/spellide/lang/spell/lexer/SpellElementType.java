package com.datechsw.spellide.lang.spell.lexer;

import com.datechsw.spellide.lang.spell.SpellFileType;
import com.datechsw.spellide.lang.spell.SpellLanguage;
import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

/**
 * Class for Spell specific lexer tokens. Each lexer token type for Spell language
 * is instance of this class.
 * 
 * @see com.datechsw.spellide.lang.spell.lexer.SpellTokenTypes
 */
public class SpellElementType extends IElementType {
    public SpellElementType(@NotNull @NonNls String debugName) {
        super(debugName, SpellFileType.INSTANCE.getLanguage());
    }

    @Override
    public String toString() {
        return SpellLanguage.LANG_NAME + ": " + super.toString();
    }
}