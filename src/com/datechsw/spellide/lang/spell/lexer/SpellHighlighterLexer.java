package com.datechsw.spellide.lang.spell.lexer;

import com.intellij.lexer.FlexAdapter;
import com.intellij.lexer.MergingLexerAdapter;

public class SpellHighlighterLexer extends MergingLexerAdapter{
    public SpellHighlighterLexer() {
        super(new FlexAdapter(new _SpellLexer((java.io.Reader)null)), SpellTokenTypes.COMMENTS);
    }
}
