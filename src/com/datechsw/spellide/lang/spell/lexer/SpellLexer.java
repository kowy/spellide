package com.datechsw.spellide.lang.spell.lexer;

import com.intellij.lexer.FlexAdapter;

import java.io.Reader;

/**
 * Initialization and configuration of Spell lexer. It uses output generated from
 * lexical generator JFlex. Use this command to generate lexer:
 *     $FLEX_PATH/bin/jflex --charat --nobak --skel $FLEX_PATH/../idea-flex.skeleton Majic.flex
 */
public class SpellLexer extends FlexAdapter {
    public SpellLexer() {
        super(new _SpellLexer((Reader)null));
    }
}