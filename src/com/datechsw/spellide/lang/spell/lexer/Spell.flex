package com.datechsw.spellide.lang.spell.lexer;

import java.util.*;
import com.intellij.lexer.FlexLexer;
import com.intellij.psi.tree.IElementType;

%%
%class _SpellLexer
%implements FlexLexer
%unicode
/* change the name of the function next_token - this is necessary for compatibility with Flex implementation in IntelliJ */
%function advance
/* change the type of return value of the function next_token - this is necessary for compatibility with Flex implementation in IntelliJ */
%type IElementType
/* what should be done when input file EOF is reached */
%eof{ return;
%eof}

%{
  private Stack<IElementType> braceCounter = new Stack <IElementType>();
%}

/* definice maker - tabulka typických terminálů ve zdrojovém souboru */
CRLF = \n | \r | \r\n
WHITE_SPACE = [ \t]
WHITE_SPACE_EOL = [ \n\r\t\f]
NON_EOL_CHARACTER = [^\n\r\f]
IDENTIFIER = [:jletter:][:jletterdigit:]*
NON_WHITE_SPACE = [^\ \t]

ONE_LINE_COMMENT = {WHITE_SPACE}* "//" .*
MULTI_LINE_COMMENT = "/*" [^*] ~"*/" | "/*" "*"+ "/"
DOCUMENTATION_COMMENT = "/**" {COMMENT_CONTENT} "*"+ "/"
COMMENT_CONTENT = ( [^*] | "*"+ [^*/] )*
COMMENT = {ONE_LINE_COMMENT} | {MULTI_LINE_COMMENT} | {DOCUMENTATION_COMMENT}

///// String literals
CHARACTER_LITERAL="'"([^\\\'\r\n]|{ESCAPE_SEQUENCE})*("'"|\\)?
STRING_LITERAL=\"([^\\\"\r\n]|{ESCAPE_SEQUENCE})*(\"|\\)?
ESCAPE_SEQUENCE="\\"[^ \r\n]

///// Integers and floats
INTEGER_LITERAL = [-]?[:digit:]+
FLOATING_POINT_LITERAL = [-]?[:digit:]+ "." [:digit:]

/* status definition - other non-standard non-terminals in a pushdown automaton */
%state MACRO_DECLARATION, MACRO_DECLARATION_VALUE
%state METHOD_DEFINITION
%state STRING, STRING_ESC, STRING_TOKEN

%%

<YYINITIAL> {
    {WHITE_SPACE_EOL}+          { return SpellTokenTypes.WHITE_SPACE; }
    {ONE_LINE_COMMENT}          { return SpellTokenTypes.COMMENT; }
    {MULTI_LINE_COMMENT}        { return SpellTokenTypes.COMMENT; }
    {DOCUMENTATION_COMMENT}     { return SpellTokenTypes.DOCUMENTATION_COMMENT; }
    "#define"                   { yybegin(MACRO_DECLARATION); return SpellTokenTypes.KEYWORD_MACRO; }
    "void"                      { yybegin(METHOD_DEFINITION); return SpellTokenTypes.KEYWORD_VOID; }
    "int"                       { yybegin(METHOD_DEFINITION); return SpellTokenTypes.KEYWORD_INT; }
    "string"                    { yybegin(METHOD_DEFINITION); return SpellTokenTypes.KEYWORD_STRING; }
    "float"                     { yybegin(METHOD_DEFINITION); return SpellTokenTypes.KEYWORD_FLOAT; }
    "date"                      { yybegin(METHOD_DEFINITION); return SpellTokenTypes.KEYWORD_DATE; }
    "duration"                  { yybegin(METHOD_DEFINITION); return SpellTokenTypes.KEYWORD_DURATION; }
    "bool"                      { yybegin(METHOD_DEFINITION); return SpellTokenTypes.KEYWORD_BOOL; }
    "uuid"                      { yybegin(METHOD_DEFINITION); return SpellTokenTypes.KEYWORD_UUID; }
    {IDENTIFIER}+               { yybegin(METHOD_DEFINITION); return SpellTokenTypes.IDENTIFIER; }
    {NON_WHITE_SPACE}           { return SpellTokenTypes.BAD_CHARACTER; }
}

<MACRO_DECLARATION> {
    {IDENTIFIER}                { yybegin(MACRO_DECLARATION_VALUE); return SpellTokenTypes.IDENTIFIER; }
    {WHITE_SPACE}+              { return SpellTokenTypes.WHITE_SPACE; }
    {NON_WHITE_SPACE}           { yybegin(MACRO_DECLARATION_VALUE); return SpellTokenTypes.BAD_CHARACTER; }
}

<MACRO_DECLARATION_VALUE> {
    .*                          { yybegin(YYINITIAL); return SpellTokenTypes.MACRO_VALUE; }
}

<METHOD_DEFINITION> {
    {ONE_LINE_COMMENT}          { return SpellTokenTypes.COMMENT; }
    {MULTI_LINE_COMMENT}        { return SpellTokenTypes.COMMENT; }
    ////////////// keywords ////////////////
    "break"                     { return SpellTokenTypes.KEYWORD_BREAK; }
    "bool"                      { return SpellTokenTypes.KEYWORD_BOOL; }
    "case"                      { return SpellTokenTypes.KEYWORD_CASE; }
    "continue"                  { return SpellTokenTypes.KEYWORD_CONTINUE; }
    "date"                      { return SpellTokenTypes.KEYWORD_DATE; }
    "duration"                  { return SpellTokenTypes.KEYWORD_DURATION; }
    "else"                      { return SpellTokenTypes.KEYWORD_ELSE; }
    "ERROR"                     { return SpellTokenTypes.KEYWORD_ERROR; }
    "false"                     { return SpellTokenTypes.KEYWORD_FALSE; }
    "FATAL"                     { return SpellTokenTypes.KEYWORD_FATAL; }
    "float"                     { return SpellTokenTypes.KEYWORD_FLOAT; }
    "for"                       { return SpellTokenTypes.KEYWORD_FOR; }
    "if"                        { return SpellTokenTypes.KEYWORD_IF; }
    "int"                       { return SpellTokenTypes.KEYWORD_INT; }
    "MILESTONE"                 { return SpellTokenTypes.KEYWORD_MILESTONE; }
    "NULL"                      { return SpellTokenTypes.KEYWORD_NULL; }
    "object"                    { return SpellTokenTypes.KEYWORD_OBJECT; }
    "return"                    { return SpellTokenTypes.KEYWORD_RETURN; }
    "SIGNIFICANT"               { return SpellTokenTypes.KEYWORD_SIGNIFICANT; }
    "string"                    { return SpellTokenTypes.KEYWORD_STRING; }
    "switch"                    { return SpellTokenTypes.KEYWORD_SWITCH; }
    "TRACE"                     { return SpellTokenTypes.KEYWORD_TRACE; }
    "true"                      { return SpellTokenTypes.KEYWORD_TRUE; }
    "uuid"                      { return SpellTokenTypes.KEYWORD_UUID; }
    "VERBOSE"                   { return SpellTokenTypes.KEYWORD_VERBOSE; }
    "void"                      { return SpellTokenTypes.KEYWORD_VOID; }
    "WARNING"                   { return SpellTokenTypes.KEYWORD_WARNING; }
    "while"                     { return SpellTokenTypes.KEYWORD_WHILE; }

    ////////////// operators /////////////
    ";"                         { return SpellTokenTypes.SEMICOLON; }
    ","                         { return SpellTokenTypes.OP_COMMA; }
    "."                         { return SpellTokenTypes.OP_DOT; }
    "..."                       { return SpellTokenTypes.OP_TRIDOT; }
    "::"                        { return SpellTokenTypes.OP_FOURDOT; }
    "+"                         { return SpellTokenTypes.OP_ADD; }
    "-"                         { return SpellTokenTypes.OP_SUB; }
    "*"                         { return SpellTokenTypes.OP_MULT; }
    "/"                         { return SpellTokenTypes.OP_DIV; }
    "%"                         { return SpellTokenTypes.OP_MODULO; }
    "=="                        { return SpellTokenTypes.OP_EQ; }
    "!="                        { return SpellTokenTypes.OP_NEQ; }
    "<="                        { return SpellTokenTypes.OP_LESS_OR_EQ; }
    "<"                         { return SpellTokenTypes.OP_LESS; }
    ">="                        { return SpellTokenTypes.OP_MORE_OR_EQ; }
    ">"                         { return SpellTokenTypes.OP_MORE; }
    "||"                        { return SpellTokenTypes.OP_LOG_OR; }
    "&&"                        { return SpellTokenTypes.OP_LOG_AND; }
    "!"                         { return SpellTokenTypes.OP_LOG_NOT; }
    "="                         { return SpellTokenTypes.OP_ASSIGN; }
    "+="                        { return SpellTokenTypes.OP_ADD_ASSIGN; }
    "-="                        { return SpellTokenTypes.OP_SUB_ASSIGN; }
    "*="                        { return SpellTokenTypes.OP_MULT_ASSIGN; }
    "/="                        { return SpellTokenTypes.OP_DIV_ASSIGN; }
    "%="                        { return SpellTokenTypes.OP_MODULO_ASSIGN; }

    ////////////// braces ////////////////
    "{"                         { braceCounter.push(SpellTokenTypes.LEFT_CURL_BRACKET); return SpellTokenTypes.LEFT_CURL_BRACKET; }
    "}"                         {
                                   if (!braceCounter.isEmpty() && SpellTokenTypes.LEFT_CURL_BRACKET == braceCounter.peek()) {
                                       braceCounter.pop(); 
                                       if (braceCounter.isEmpty()) yybegin(YYINITIAL);
                                       return SpellTokenTypes.RIGHT_CURL_BRACKET;
                                   } else {
                                       return SpellTokenTypes.BAD_CHARACTER;
                                   }
                                }
    "("                         { braceCounter.push(SpellTokenTypes.LEFT_PARENTHESIS); return SpellTokenTypes.LEFT_PARENTHESIS; }
    ")"                         {
                                    if (!braceCounter.isEmpty() && SpellTokenTypes.LEFT_PARENTHESIS == braceCounter.peek()) {
                                        braceCounter.pop(); return SpellTokenTypes.RIGHT_PARENTHESIS;
                                    } else {
                                        return SpellTokenTypes.BAD_CHARACTER;
                                    }
                                }
    "["                         { braceCounter.push(SpellTokenTypes.LEFT_SQ_BRACKET); return SpellTokenTypes.LEFT_SQ_BRACKET; }
    "]"                         {
                                   if (!braceCounter.isEmpty() && SpellTokenTypes.LEFT_SQ_BRACKET == braceCounter.peek()) {
                                       braceCounter.pop(); return SpellTokenTypes.RIGHT_SQ_BRACKET;
                                   } else {
                                       return SpellTokenTypes.BAD_CHARACTER;
                                   }
                                }

    {INTEGER_LITERAL}           { return SpellTokenTypes.INTEGER_NUMBER; }
    {FLOATING_POINT_LITERAL}    { return SpellTokenTypes.FP_NUMBER; }

    {IDENTIFIER}                { return SpellTokenTypes.IDENTIFIER; }
    {STRING_LITERAL} |
    {CHARACTER_LITERAL}         { return SpellTokenTypes.STRING; }
    
    {WHITE_SPACE_EOL}+          { return SpellTokenTypes.WHITE_SPACE; }
    {NON_EOL_CHARACTER}         { return SpellTokenTypes.BAD_CHARACTER; }
 }
