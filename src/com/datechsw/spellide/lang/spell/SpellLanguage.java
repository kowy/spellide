package com.datechsw.spellide.lang.spell;

import com.datechsw.spellide.lang.spell.highlighter.SpellSyntaxHighlighter;
import com.intellij.lang.Language;
import com.intellij.openapi.fileTypes.SingleLazyInstanceSyntaxHighlighterFactory;
import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import com.intellij.openapi.fileTypes.SyntaxHighlighterFactory;
import org.jetbrains.annotations.NotNull;

/**
 * Top level configuration of Spell highlighter, language, file type etc.
 */
public class SpellLanguage extends Language {
    @NotNull
    public static SpellLanguage INSTANCE = new SpellLanguage();
    public static final String LANG_NAME = "Spell";

    private SpellLanguage() {
        super(LANG_NAME, SpellFileType.MIME_TYPE);
        SyntaxHighlighterFactory.LANGUAGE_FACTORY.addExplicitExtension(this, new SingleLazyInstanceSyntaxHighlighterFactory() {
            @NotNull
            @Override
            protected SyntaxHighlighter createHighlighter() {
                return new SpellSyntaxHighlighter();
            }
        });

    }
}
