package com.datechsw.spellide.lang;

import com.datechsw.spellide.lang.majic.MajicFileType;
import com.datechsw.spellide.lang.spell.SpellFileType;
import com.intellij.openapi.fileTypes.FileTypeConsumer;
import org.jetbrains.annotations.NotNull;

/**
 * Factory exposing definition of all languages implemented in SpellIde
 */
public class FileTypeFactorySpecific extends com.intellij.openapi.fileTypes.FileTypeFactory {
    @Override
    public void createFileTypes(@NotNull FileTypeConsumer consumer) {
        consumer.consume(SpellFileType.INSTANCE, SpellFileType.DEFAULT_EXTENSION);
        for (String extension : MajicFileType.DEFAULT_EXTENSIONS) {
            consumer.consume(MajicFileType.INSTANCE, extension);
        }
    }
}
