package com.datechsw.spellide;

import com.intellij.CommonBundle;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.PropertyKey;

import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.util.ResourceBundle;

/**
 * Java Class providing Spell specific localization messages
 */
public final class SpellIdeBundle {
    private static Reference<ResourceBundle> ourBundle;
    @NonNls private static final String PATH_TO_BUNDLE = "messages.SpellIdeBundle";

    private SpellIdeBundle() {}

    public static String message(@PropertyKey(resourceBundle = PATH_TO_BUNDLE)String key, Object... params) {
        return CommonBundle.message(getBundle(), key, params);
    }

    private static ResourceBundle getBundle() {
        ResourceBundle retval = null;
        if (ourBundle != null) retval = ourBundle.get();
        if (retval == null) {
            retval = ResourceBundle.getBundle(PATH_TO_BUNDLE);
            ourBundle = new SoftReference<ResourceBundle>(retval);
        }

        return retval;
    }

}